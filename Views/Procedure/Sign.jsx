import React, { Component } from 'react';
import ExpoPixi from 'expo-pixi';
import { ButtonMain, TextBody, Divider, Image } from '../../Components';
import { StyleSheet, View, Modal } from 'react-native';
import { connect } from 'react-redux';
import { appActions } from '../../actions/appActions';
import { appStore } from '../../selectors';
import { routes } from '../../constants';

class Sign extends Component {

  constructor(props) {
    super(props);
    this.state = {
      showReview: false,
      signatureResult: {}
    }
  }

  clearCanvas = () => {
    this.refs.signatureCanvas.clear()
  }

  saveCanvas = () => {
    const { setPackageRequest, packageRequest,setIsLoading } = this.props;
    let p = packageRequest;
    setIsLoading(true);
    //const signature_result = await
    this.refs.signatureCanvas.takeSnapshotAsync({
      format: 'png', // other option is jpg
      quality: 0.5, // quality 0 for very poor 1 for very good
      result: 'file' // 
    }).then(signature_result => {
      setIsLoading(false);

      p["sign"] = {
        uri: signature_result.uri,
        type: "image/*",
        name: "sign_identity" + signature_result.uri.substring(signature_result.uri.lastIndexOf("."))
      };
      setPackageRequest(Object.assign({}, p));
      //console.log("-->",signature_result);

      // this.setState({
      //   signatureResult: signature_result,
      //   showReview: true,
      // });

      this.handleAccept();

    })

  }

  handleAccept = () => {
    const { packageRequest, infoUser, sendPackageRequest, navigation, setFolio } = this.props;
    let p = packageRequest;
    p["solicitant"] = infoUser.id;
    sendPackageRequest(p).then(result => {
      setFolio(result.payload ? "".concat(result.payload.folio) : "");
      navigation.replace(routes.END_PROCEDURE);
    }, error => {
      if (error === -1) {//token is not valid
        navigation.popToTop();
      }
    });
  }

  handleRepeatSing = () => {
    this.clearCanvas();
    this.setState({
      signatureResult: {},
      showReview: false,
    })
  }


  render() {
    const { showReview, signatureResult } = this.state;

    return (
      <React.Fragment>
        <Modal style={styles.reviewRoot}
          transparent={true}
          visible={showReview}>
          <View style={styles.reviewContent} >
            <View style={styles.reviewBody} >
              <View style={styles.reviewTitle} >
                <TextBody textBlue >{'Revisa tu firma.'}</TextBody>
                <Divider />
                <TextBody textParagraph >{'Asegúrate que sea lo más parecida a tu documento de identificación.'}</TextBody>
                <TextBody textParagraph >{'Puedes repetir la firma si es necesario.'}</TextBody>
              </View>
              <View style={styles.reviewImage} >
                {/* <ScrollView style={styles.reviewScroll} > */}
                {/* {signatureResult.uri &&
                  <Image
                    style={styles.image}
                    source={{
                      uri: decodeURIComponent(signatureResult.uri),
                      isStatic: true
                    }}
                  />
                } */}
                {/* </ScrollView> */}
              </View>
              <View style={styles.reviewButtons} >
                <ButtonMain
                  buttonCenter
                  buttonRed
                  buttonPadding
                  onPress={this.handleRepeatSing}
                  text={'Repetir la firma'} />
                <ButtonMain
                  buttonBlue
                  buttonCenter
                  buttonPadding
                  onPress={this.handleAccept}
                  text={'Eviar Solicitud'} />
              </View>
            </View>
          </View>
        </Modal>


        <View style={styles.root} >
          <View style={styles.body}>
            <TextBody textBold textCenter textParagraph >{'Firma de solicitud'}</TextBody>
            <TextBody textParagraph >{'Dibuja tu firma dentro del recuadro y presiona el botón '}
              <TextBody textBold>
                {'Aceptar'}
              </TextBody>
            </TextBody>
            <View style={styles.signContain}>
              <ExpoPixi.Signature
                style={styles.sign}
                ref='signatureCanvas' //Important to be able to call this obj
                strokeWidth={2} // thickness of the brush
                strokeAlpha={0.5} // opacity of the brush                  
              />
            </View>
          </View>
          <View style={styles.footer}>
            <ButtonMain buttonRed text={'Limpiar'} onPress={this.clearCanvas} />
            <ButtonMain buttonBlue text={'Aceptar'} onPress={this.saveCanvas} />
          </View>
        </View>
      </React.Fragment>
    )
  }
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    padding: 15,
    fontSize: 22,
    backgroundColor: '#f5f5f5',
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  body: {
    flex: 21,
    justifyContent: "center",
  },
  footer: {
    flex: 3,
    justifyContent: "center",
    flexDirection: "row",
    justifyContent: "space-around",
    alignContent: "center",
    alignItems: "center",
  },
  signContain: {
    borderWidth: 1,
    borderRadius: 15,
    backgroundColor: "#FFFFFF",
    flex: 1,
    padding: 5,
  },
  sign: {
    flex: 1,
    backgroundColor: "#FFFFFF",
  },

  reviewRoot: {
    flex: 1,
  },
  reviewContent: {
    flex: 1,
    padding: 15,
    backgroundColor: '#000000CB',
    alignItems: 'center',
    justifyContent: 'center',
  },
  reviewBody: {
    flex: 1,
    padding: 20,
    alignContent: "center",
    justifyContent: "space-between",
    backgroundColor: "#FFFFFF",
    borderRadius: 30,
  },
  reviewImage: {
    flex: 1,
    justifyContent: "center",
    alignSelf: "stretch",
  },
  reviewButtons: {
    flexDirection: "column",
    alignSelf: "center",
  },
  image: {
    alignSelf: "stretch",
    flex: 1,
  }


});

const mapStateToProps = (state) => ({
  packageRequest: appStore.getPackageRequest(state),
  infoUser: appStore.getInfoUser(state),
  //typePerson: appStore.getTypePerson(state),
});

const mapDispathToProps = dispatch => ({
  sendPackageRequest: (payload) => dispatch(appActions.sendPackageRequest(payload)),
  clearAll: (payload) => dispatch(appActions.clearAll(payload)),
  setFolio: (payload) => dispatch(appActions.setFolio(payload)),
  setPackageRequest: (payload) => dispatch(appActions.setPackageRequest(payload)),
  setIsLoading: (payload) => dispatch(appActions.setIsLoading(payload)),
});

Sign.propTypes = {

};

export default connect(mapStateToProps, mapDispathToProps)(Sign);