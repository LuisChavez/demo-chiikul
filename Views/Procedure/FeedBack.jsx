import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, StyleSheet, TouchableOpacity, TextInput } from 'react-native';
import { TextBody, ButtonMain } from '../../Components';
import { MainContent } from '../Layouts';
import { pallete } from '../../constants';
import { Icon, Star, StarOk } from '../../Components/Icons';
import RadioForm from 'react-native-simple-radio-button';

class FeedBack extends Component {

  constructor(props) {
    super(props);
    this.state = {
      level1: 0,
      question1: null,
    }
  }

  handleNext = () => {
    this.props.navigation.popToTop();
  }

  renderLevel = (level, styles) => {
    let i = 0;
    let objects = [];
    for (i = 1; i < 6; i++) {
      const lev = i;
      if (i > level) {
        objects.push(<TouchableOpacity key={lev} style={{ marginLeft: 2, marginRight: 2 }} onPress={() => { this.setState({ level1: lev }) }} >
          <Star style={styles} />
        </TouchableOpacity>);
      }
      else {
        objects.push(<TouchableOpacity key={lev} style={{ marginLeft: 2, marginRight: 2 }} onPress={() => { this.setState({ level1: lev }); }} >
          <StarOk style={styles} />
        </TouchableOpacity>);
      }
    }
    return objects;
  }



  render() {
    const { level1 } = this.state;

    return (
      <View style={styles.root} >
        <View style={styles.body}>
          <TextBody textBold textCenter textMedium textParagraph >{'Esperamos que tu experiencia con la aplicación haya sido satisfactoria. Puedes ayudarnos a mejorar calificando la aplicación, y un comentario si así lo deseas'}</TextBody>
          <MainContent>            
            <View style={{ padding: 15 }}>
              <TextBody textParagraph textBlue textCenter >{'¿Qué tan satisfecho estás con la aplicación?'}
              </TextBody>
              <View style={styles.stars} >
                {this.renderLevel(level1, styles.icon)}
              </View>
              <TextBody textParagraph textBlue textCenter>
                {'¿Recomendarías esta aplicación a alguien más?'}
              </TextBody>
              <RadioForm style={styles.radio}
                radio_props={[
                  { label: 'Sí', value: true },
                  { label: 'No', value: false }
                ]}
                //initial={null}
                formHorizontal={true}
                labelHorizontal={false}
                buttonColor={pallete.main}
                animation={true}
                onPress={() => { }}
              />
              <TextBody textBlue>
                {'Comentarios'}
              </TextBody>
              <TextInput
                style={styles.textInput}
                multiline={true}                
                //value={textInput}
                //onChangeText={(t) => setTextInput(t)}
                //keyboardType={keyboardType}
                maxLength={1024}
              //autoCapitalize={autoCapitalize}
              />
            </View>
          </MainContent>
        </View>
        <View style={styles.footer}>
          <ButtonMain buttonBlue buttonBig buttonCenter text={'Enviar y finalizar'} onPress={this.handleNext} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    padding: 15,
    backgroundColor: pallete.back,
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  body: {
    flex: 19,
    justifyContent: "center"
  },
  footer: {
    flex: 3,
    justifyContent: "center"
  },
  icon: {
    // backgroundColor: pallete.main,
    width: 64,
    height: 64,
  },
  stars: {
    flexDirection: "row",
    alignItems: "center",
    alignContent: "center",
    alignSelf: "center",
    marginBottom: 30,
  },
  radio: {
    flexDirection: "row",
    alignItems: "center",
    alignContent: "center",
    alignSelf: "center",
    marginBottom: 30,
  },
  textInput: {
    backgroundColor: "#FFFFFF",
    height: 80,
    borderRadius: 10,
    paddingLeft: 10,
    paddingTop: 10,
    borderColor: '#000000',
    borderWidth: 1,
    textAlignVertical:"top"
  },  
});

FeedBack.propTypes = {

};

export default FeedBack;