import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, StyleSheet } from 'react-native';
import { TextBody, ButtonMain } from '../../Components';
import { pallete, routes } from '../../constants';


const PresentationNext = ({ navigation }) => {

  const handleNext = () => {
    navigation.replace(routes.STEP_ONE);
  }

  const goDocumentation = () => {
    navigation.replace(routes.DOCUMENTATION);
  }


  return (
    <View style={styles.root} >
      <View style={styles.body}>
        <TextBody style={{ paddingBottom: 30 }} >{'Ten a la mano la documentación requerida para el trámite.'}</TextBody>
        <TextBody style={{ paddingBottom: 30 }} >{'Si desconoces cual es o tienes duda, puedes consultarlo en la sección '}
          <TextBody textBlue textSubline onPress={goDocumentation} >{'Requisitos'}</TextBody>.
          </TextBody>
      </View>
      <View style={styles.footer}>
        <ButtonMain
          buttonBlue
          buttonPadding
          buttonCenter
          text={'Continuar'}
          onPress={handleNext} />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    padding: 15,
    fontSize: 22,
    backgroundColor: '#f5f5f5',
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  body: {
    flex: 10,
    fontSize: 22,
  },
  footer: {
    flex: 2,
    justifyContent: "center"
  },
  buttonBlue: {
    backgroundColor: pallete.main,
    alignSelf: "center",
    justifyContent: "center",
    padding: 40,
    paddingTop: 10,
    paddingBottom: 10,
  },
  textUnderline: {
    textDecorationStyle: "dashed",
  }
});

PresentationNext.propTypes = {

};

export default PresentationNext;