import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Modal, View, TextInput, StyleSheet } from 'react-native';
import TextBody from './TextBody';
import ButtonMain from './ButtonMain';
import { pallete } from '../constants';
import { Icon, Replay } from './Icons';


const ModalInput = ({ show, fnOk, title, textInit, keyboardType, max, autoCapitalize }) => {

  const [showCaptcha, setShowCaptcha] = useState(show);
  const [textInput, setTextInput] = useState(textInit);

  const handleCloseModal = () => {
    fnOk(textInput.trim());
  }

  useEffect(() => {
    setShowCaptcha(show);
  }, [show]);

  useEffect(() => {
    setTextInput(textInit);
  }, [textInit]);

  return (
    <Modal style={styles.modal}
      animationType="slide"
      transparent={true}
      visible={showCaptcha}>
      <View style={styles.root} >
        <View style={styles.body} >
          <TextBody textCenter textMedium textBold >
            {title}
          </TextBody>
          <TextInput style={styles.textInput}
            value={textInput}
            onChangeText={(t) => setTextInput(t)}
            keyboardType={keyboardType}
            maxLength={max}
            autoCapitalize={autoCapitalize}
          />         
          <ButtonMain
            buttonBlue
            buttonCenter
            buttonPadding
            onPress={handleCloseModal}
            disabled={textInput.length === 0}
            text={'CONTINUAR'} />
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  modal: {
    flex: 1,
  },
  root: {
    flex: 1,
    padding: 15,
    backgroundColor: '#000000CB',
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  body: {
    padding: 20,
    alignContent: "center",
    justifyContent: "center",
    backgroundColor: "#FFFFFF",
    borderRadius: 30,
  },
  textInput: {
    backgroundColor: "#FFFFFF",
    height: 40,
    borderRadius: 10,
    paddingLeft: 10,
    borderColor: '#000000',
    borderWidth: 1,
  },
  image: {
    height: 150,
    resizeMode: 'contain',
  },
  btnChange: {
    flexDirection: "row",
    alignItems: "center",
    paddingTop: 0,
    paddingBottom: 0,
    paddingLeft: 5,
    paddingRight: 5,
  }
});

ModalInput.defaultProps = {
  show: false,
  fnOk: () => { },
  title: "",
  keyboardType: "default",
  max: 1024,
  autoCapitalize: "none",
}

ModalInput.propTypes = {
  title: PropTypes.string,
  keyboardType: PropTypes.string,
  max: PropTypes.number,
  autoCapitalize: PropTypes.string,

};

export default ModalInput;