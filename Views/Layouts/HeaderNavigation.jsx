import React from 'react';
import PropTypes from 'prop-types';
import { View, Image, StyleSheet } from 'react-native';
import { TextBody } from '../../Components';
import { Icon, Paper, User, Security, Folder } from '../../Components/Icons';
import { pallete } from '../../constants';

const HeaderNavigation = ({ scene, previous, navigation }) => {

  const { options } = scene.descriptor;

  const title =
    (options && (
      options.headerTitle !== undefined
        ? options.headerTitle
        : options.title !== undefined
          ? options.title
          : scene.route.name));
  
  const { typeIcon } = scene.route.params !== undefined && scene.route.params ;

  return (
    <View style={styles.head} >
      <View style={styles.title} >
        {typeIcon &&
          <Icon style={{ backgroundColor: pallete.main }} width={42} height={42} round >
            {
              (typeIcon === "Paper" && <Paper width={32} height={32} />) ||
              (typeIcon === "User" && <User width={32} height={32} />) ||
              (typeIcon === "Security" && <Security width={32} height={32} />) ||
              (typeIcon === "Folder" && <Folder width={32} height={32} />)
            }
          </Icon>
        }
        <TextBody textBold textMedium={typeIcon !== undefined} style={{ marginBottom: 0 }} >{title && title}</TextBody>
      </View>
      <View style={styles.logo}  >
        <Image style={styles.image}
          source={require("../../assets/images/sat.png")} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  head: {
    flexDirection: "row",
    height: 80,
    backgroundColor: "#FFFFFF"
  },
  logo: {
    flex: 10,
  },
  title: {
    flex: 11,
    justifyContent: "flex-start",
    paddingLeft: 5,
    flexDirection: "row",
    alignItems: "center"
  },
  image: {
    resizeMode: "stretch",
    flex: 1,
    width: 200,
    alignSelf: "flex-end",
  }
});

HeaderNavigation.propTypes = {

};

export default HeaderNavigation;