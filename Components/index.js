import * as Icons from './Icons';

export { default as TextTitle } from './TextTitle';
export { default as TextBody } from './TextBody';
export { default as ButtonMain } from './ButtonMain';
export { default as IconButton } from './IconButton';
export { default as CameraView } from './CameraView';
export { default as ModalCaptcha } from './ModalCaptcha';
export { default as CameraVideo } from './CameraVideo';
export { default as Divider } from './Divider';
export { default as ModalInput } from './ModalInput';

export { Icons };
