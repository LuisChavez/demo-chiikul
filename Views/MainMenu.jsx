import React from 'react';
import PropTypes from 'prop-types';
import { View, StyleSheet, BackHandler } from 'react-native';
import { TextBody, ButtonMain } from '../Components';
import { routes } from '../constants';
import MainContent from './Layouts/MainContent';
import { appActions } from '../actions/appActions';
import { connect } from 'react-redux';

const MainMenu = ({ navigation, clearAll }) => {

  const handleProcedure = () => {
    navigation.navigate(routes.PRESENTATION);
  }

  const handleDocumentation = () => {
    navigation.navigate(routes.DOCUMENTATION);
  }

  return (
    <MainContent>
      <View style={styles.root} >
        <View style={styles.head}>
          <TextBody textBold textCenter >{'Solicitud de Generación o Actualización de Contraseña.'}</TextBody>
        </View>
        <View style={styles.body}>
          <ButtonMain buttonFull text="INICIAR TRÁMITE" onPress={handleProcedure} />
          <ButtonMain buttonFull text="REQUISITOS" onPress={handleDocumentation} />
          <ButtonMain buttonFull text="Salir" onPress={() => { clearAll().then(s => { BackHandler.exitApp() })  /*BackHandler.exitApp()*/ }} />
        </View>
        <View style={styles.footer}>
        </View>
      </View>
    </MainContent>
  );
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    padding: 15,
    fontSize: 22,
    backgroundColor: '#f5f5f5',
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  head: {
    flex: 1,
    fontSize: 22,
  },
  body: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'stretch',
    paddingTop: 50,
  },
  footer: {
    flex: 2,
  },

});

const mapDispathToProps = dispatch => ({
  clearAll: (payload) => dispatch(appActions.clearAll(payload)),
});


MainMenu.propTypes = {

};

export default connect(null, mapDispathToProps)(MainMenu);