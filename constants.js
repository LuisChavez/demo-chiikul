import Constants from 'expo-constants';

const url_base_api = (() => {
  return Constants.manifest.extra.urlApi || "http://localhost:8000";
})()

const url_base_captcha = (() => {
  return Constants.manifest.extra.urlCaptcha || "http://localhost:8080";
})()

export const url_document = (() => {
  return Constants.manifest.extra.urlDocument || "";
})()

export const pallete = {
  back: "#F5F5F5",
  dark: "#707070",
  main: "#0058b8",
  second: "#b8005c",
  fontColor: "#5A5A5A",
}

export const appTypeActions = {
  NEXT_STEP: "NEXT_STEP",
  BACK_STEP: "BACK_STEP",
  RESET_STEP: "RESET_STEP",
  SET_SOLICITANT: "SET_SOLICITANT",
  REGISTER_SOLICITANT: "REGISTER_SOLICITANT",
  SET_INFO_USER: "SET_INFO_USER",
  CONFIRM_SOLICITANT: "CONFIRM_SOLICITANT",
  SAVE_TOKEN_USER: "SAVE_TOKEN_USER",
  FETCH_TAXPAYER_LIST: "FETCH_TAXPAYER_LIST",
  SET_TAXPAYER_LIST: "SET_TAXPAYER_LIST",
  SET_VALIDATED_USER: "SET_VALIDATED_USER",
  SET_TYPE_PERSON: "SET_TYPE_PERSON",
  SET_PACKAGE_REQUEST: "SET_PACKAGE_REQUEST",
  SEND_PACKAGE_REQUEST: "SEND_PACKAGE_REQUEST",
  CLEAR_STORE: "CLEAR_STORE",
  SET_LOADING: "SET_LOADING",
  FETCH_CAPTCHA: "FETCH_CAPTCHA",
  SET_ERROR: "SET_ERROR",
  SET_INDEX_DOCUMENT: "SET_INDEX_DOCUMENT",
  SET_FOLIO: "SET_FOLIO",
  SAVE_STORAGE_USER: "SAVE_STORAGE_USER",
  SEND_CODES: "SEND_CODES",

}

export const url_api = {
  root: url_base_api,
  solicitant: {
    resource: `${url_base_api}solicitant/`,
    sendValidation: `${url_base_api}solicitant/{id}/validate/`,
    sendSolicitude: `${url_base_api}request/`,
  },
  catalog: {
    taxpayerTypes: {
      resource: `${url_base_api}taxpayer_types/`,
    }
  },
  captcha: {
    resource: `${url_base_captcha}get_captcha`,
  },
  smsService: {
    resource: `${url_base_api}send_codes/`,
  },
}

export const routes = {
  MAIN_MENU: "MAIN_MENU",
  PRESENTATION: "PRESENTATION",
  PRESENTATION_NEXT: "PRESENTATION_NEXT",
  STEP_ONE: "STEP_ONE",
  DOCUMENTATION: "DOCUMENTATION",
  STEP_ONE_SMS: "STEP_ONE_SMS",
  STEP_ONE_TYPE: "STEP_ONE_TYPE",
  STEP_TWO: "STEP_TWO",
  CAMERA_VIEW: "CAMERA_VIEW",
  FILES: "FILES",
  IDENTITY: "IDENTITY",
  TAKE_VIDEO: "TAKE_VIDEO",
  REVIEW_INFO: "REVIEW_INFO",
  END_PROCEDURE: "END_PROCEDURE",
  SING: "SING",
  FEED_BACK: "FEED_BACK",

}