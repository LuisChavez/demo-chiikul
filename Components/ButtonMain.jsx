import React from 'react';
import PropTypes from 'prop-types';
import { Text, StyleSheet, TouchableOpacity } from 'react-native';
import { pallete } from '../constants';
import TextBody from './TextBody';


const ButtonMain = ({ text, style, buttonBlue, buttonBig, buttonRed, buttonFull, buttonCenter,
  buttonPadding,
  textBlue, textTitle, textBold, textSubline, textParagraph, textSmall, children, ...props }) => {
  return (

    <TouchableOpacity style={Object.assign({}, styles.root,
      buttonBlue && styles.buttonBlue,
      buttonRed && styles.buttonRed,
      buttonBig && styles.buttonBig,
      buttonFull && styles.buttonFull,
      buttonCenter && styles.buttonCenter,
      buttonPadding && styles.buttonPadding,
      props.disabled && styles.disabled,
      style)}
      {...props}
      activeOpacity={0.8}
      underlayColor="#0000000F"
    >
      <React.Fragment>
        {children && children}
        <TextBody
          textSmall={textSmall}
          textBlue={textBlue}
          textTitle={textTitle}
          textBold={textBold} 
          textSubline={textSubline} 
          textParagraph={textParagraph}
          style={Object.assign({}, { ...styles.text }, (buttonBlue || buttonRed) && { color: "#FFFFFF" })} >
          {text && text}
        </TextBody>
      </React.Fragment>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  root: {
    backgroundColor: "#FFFFFF",
    shadowColor: '#29000000',
    shadowRadius: 5,
    paddingTop: 5,
    paddingBottom: 5,
    paddingLeft: 15,
    paddingRight: 15,
    marginTop: 20,
    borderRadius: 10,
    alignSelf: "flex-start",
    justifyContent: "center",
  },
  text: {
    color: pallete.dark,
    //fontSize: 22,
    textAlign: 'center',
  },
  buttonBlue: {
    backgroundColor: pallete.main,
  },
  buttonBig: {
    paddingLeft: 40,
    paddingRight: 40,
    paddingTop: 10,
    paddingBottom: 10,
  },
  buttonRed: {
    backgroundColor: pallete.second,
  },
  buttonFull: {
    alignSelf: "stretch",
    justifyContent: "center",
    paddingTop: 25,
    paddingBottom: 25,
  },
  buttonCenter: {
    justifyContent: "center",
    alignSelf: "center",
  },
  buttonPadding: {
    paddingLeft: 25,
    paddingRight: 25,
    paddingTop: 10,
    paddingBottom: 10,
  },
  disabled: {
    opacity: 0.6,
  }
})

ButtonMain.defaultProps = {
  style: {},
};

ButtonMain.propTypes = {

};

export default ButtonMain;