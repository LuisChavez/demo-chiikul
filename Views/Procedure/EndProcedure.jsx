import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import { View, StyleSheet, BackHandler } from 'react-native';
import { TextBody, ButtonMain } from '../../Components';
import { pallete, routes } from '../../constants';
import { connect } from 'react-redux';
import { appActions } from '../../actions/appActions';
import { useFocusEffect } from '@react-navigation/native';
import { MainContent } from '../Layouts';
import { appStore } from '../../selectors';


const EndProcedure = ({ clearAll, navigation, folio }) => {

  const handleNext = () => {
    clearAll().then(result => {
      navigation.popToTop();
    });
  }

  const handleFeedBack = () => {
    clearAll().then(result => {
      navigation.replace(routes.FEED_BACK);
    });
  }

  useFocusEffect(
    useCallback(() => {
      const onBackPress = () => {
        handleNext();
        return true;
      };

      BackHandler.addEventListener('hardwareBackPress', onBackPress);

      return () =>
        BackHandler.removeEventListener('hardwareBackPress', onBackPress);
    })
  );

  return (

    <View style={styles.root} >
      <View style={styles.body}>
        <TextBody textBold textCenter textParagraph >{'LA SOLICITUD FUE REGISTRADA CON ÉXITO.'}</TextBody>
        <MainContent>
          <View style={{ padding: 15 }}>
            <TextBody textParagraph textBold >{'El folio de su trámite es: '}
              <TextBody >{folio ? folio : ''}</TextBody>
            </TextBody>
            <TextBody style={{ marginTop: 30 }} textCenter textParagraph >
              {'La información de su trámite fue enviada a los medios de contacto registrados.'}
            </TextBody>
            <TextBody textCenter>
              {'El SAT realizará la revisión correspondiente y en un término máximo de 3 días hábiles posteriores a esta fecha, recibirá la respuesta correspondiente.'}
            </TextBody>
          </View>
        </MainContent>
      </View>
      <View style={styles.footer}>
        <ButtonMain buttonBlue buttonBig buttonCenter text={'Responder encuesta'} onPress={handleFeedBack} />
      </View>
      <View style={styles.footer}>
        <ButtonMain buttonBlue buttonBig buttonCenter text={'Terminar'} onPress={handleNext} />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    padding: 15,
    backgroundColor: pallete.back,
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  body: {
    flex: 10,
    justifyContent: "center"
  },
  footer: {
    flex: 2,
    justifyContent: "center"
  },
  buttonBlue: {
    backgroundColor: pallete.main,
    alignSelf: "center",
    justifyContent: "center",
    padding: 40,
    paddingTop: 10,
    paddingBottom: 10,
  },
});

const mapDispathToProps = dispatch => ({
  clearAll: (payload) => dispatch(appActions.clearAll(payload))
});

const mapStateToProps = (state) => ({
  folio: appStore.getFolio(state),
});

EndProcedure.propTypes = {
  folio: PropTypes.string,
};

export default connect(mapStateToProps, mapDispathToProps)(EndProcedure);