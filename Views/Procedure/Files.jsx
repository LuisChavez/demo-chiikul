import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, StyleSheet, ScrollView, Image, SafeAreaView, Modal, TouchableOpacity } from 'react-native';
import { TextBody, ButtonMain, Divider } from '../../Components';
import { pallete } from '../../constants';
import * as Permissions from 'expo-permissions';
import * as ImagePicker from 'expo-image-picker';
import * as DocumentPicker from 'expo-document-picker';


class Files extends Component {

  constructor(props) {
    super(props);
    this.state = {
      files: [],
      allow: false,
      showReview: false,
      showMsg: false,
    }
  }

  handleAccept = () => {
    const { fnOk } = this.props;
    if (fnOk !== undefined) {
      fnOk(this.state.files);
    }
  }

  handleOpenReview = () => {
    this.setState({ showReview: true });
  }

  handleCloseReview = () => {
    this.setState({ showReview: false });
  }

  renderFiles = () => {
    const { files } = this.state;
    const components = [];
    let cont = 0, rows = 0;
    while (cont < files.length) {
      if (cont % 3 === 0) {
        files.push(<View style={styles.row} />);
        rows++;
      }
      else {
        files[rows - 1].push(<Image style={styles.imageFile} />);
      }
      cont++;
    }
    return components;
  }

  requestPemissions = async () => {
    const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
    this.setState({ allow: (status === 'granted') });
  }

  selectImage = async (index) => {
    const result = await ImagePicker.launchImageLibraryAsync({
      allowsEditing: false,
      aspect: [4, 3],
      base64: true,
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
    });
    if (result.cancelled === false) {
      let files = this.state.files;
      files[index].type = result.type;
      files[index].uri = result.uri;
      files[index].base64 = result.base64;
      this.setState({ files: files });
    }
  }

  selectFile = async (index) => {
    const result = await DocumentPicker.getDocumentAsync({
      type: "image/*",
      copyToCacheDirectory: false,
      multiple: true,
    });
    if (result.type === 'success') {
      let files = this.state.files;
      files[index].uri = result.uri;
      this.setState({ files: files });
    }
  }

  handleCloseMessage = () => {
    this.setState({ showMsg: false });
  }

  createEmptyFiles = (n = 1) => {
    let list = [], i = 0;
    for (i = 0; i < n; i++) {
      list.push({})
    }
    return list;
  }

  async componentDidMount() {    
    await this.requestPemissions();
    this.setState({ showMsg: false, files: this.createEmptyFiles(this.props.typeSelected.pages) });
  }

  render() {

    const { files, allow, showReview, showMsg } = this.state;
    const { nameDocument = "" } = this.props;

    return (
      <React.Fragment>
        

        <Modal style={styles.reviewRoot}
          transparent={true}
          visible={showReview}>
          <View style={styles.reviewContent} >
            <View style={styles.reviewBody} >
              <View style={styles.reviewTitle} >
                <TextBody textBlue >{'Revisión de documento.'}</TextBody>
                <Divider style={{ marginBottom: 15 }} />
                <TextBody textMedium textParagraph >{'Asegúrate que la imagen sea clara y los datos del documento se vean correctamente.'}</TextBody>
                <TextBody textMedium textParagraph >{'Adjunta otro documento si es necesario.'}</TextBody>
              </View>

              <ScrollView style={styles.fileScroll} >
                {/* <View style={styles.reviewImagesContent} > */}
                {
                  files.map((f, index) => (
                    <Image key={index} style={styles.reviewImage} source={{ uri: f.uri }} />
                  ))
                }
                {/* </View> */}
              </ScrollView>

              <View style={styles.reviewButtons} >
                <ButtonMain
                  buttonRed
                  buttonCenter
                  buttonPadding
                  onPress={this.handleCloseReview}
                  text={'Repetir'} />
                <ButtonMain
                  buttonBlue
                  buttonCenter
                  buttonPadding
                  onPress={this.handleAccept}
                  text={'Continuar'} />
              </View>

            </View>
          </View>
        </Modal>

        <Modal style={styles.modalRoot}
          transparent={true}
          visible={showMsg}>
          <View style={styles.modalContent} >
            <View style={styles.modalBody} >
              <TextBody style={{ textAlign: "center" }}>{'Selecciona todas las imágenes correspondientes al documento '}
                <TextBody style={{ fontWeight: "bold" }}>
                  {nameDocument && nameDocument}
                </TextBody>.
              </TextBody>
              <ButtonMain
                buttonCenter
                buttonPadding
                buttonBlue
                onPress={this.handleCloseMessage}
                text={'Entendido'} />
            </View>
          </View>
        </Modal>

        <View style={styles.root} >
          <View style={styles.content} >
            {allow === false &&
              <React.Fragment>
                <TextBody>
                  {'Es necesario proporciones los permisos de acceso a tus archivos'}
                </TextBody>
                <ButtonMain text={"Volver a intenter"} onPress={this.requestPemissions} />
              </React.Fragment>
            }
            {allow === true &&
              <React.Fragment>
                <ButtonMain buttonBlue style={styles.button} text={"Siguiente"}
                  disabled={files.reduce((l, c) => (l + c.uri ? 1 : 0), 0) === 0}
                  onPress={this.handleOpenReview} />
                <SafeAreaView style={styles.fileScroll}>
                  <ScrollView horizontal={false} >
                    {
                      files.map((f, index) => (
                        <TouchableOpacity style={styles.touch} key={index} onPress={() => this.selectImage(index)}>
                          <React.Fragment>
                            {/* <TextBody>{`Hoja ${index+1}`}</TextBody> */}
                            <Image style={styles.imageFile} source={{ uri: f.uri }} />
                          </React.Fragment>
                        </TouchableOpacity>
                      ))
                    }
                  </ScrollView>
                </SafeAreaView>
              </React.Fragment>
            }
          </View>
        </View>
        
      </React.Fragment>
    );
  }
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    padding: 15,
    fontSize: 22,
    backgroundColor: '#f5f5f5',
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  content: {
    flex: 1,
    padding: 15,
    backgroundColor: pallete.back,
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  body: {
    padding: 20,
    alignContent: "center",
    justifyContent: "center",
    borderRadius: 30,
    marginBottom: 30,
    marginTop: 30,
  },
  fileScroll: {
    flex: 1,
    padding: 0,
    alignSelf: "stretch",
    alignContent: "stretch",
  },
  button: {
    alignSelf: "flex-end",
    justifyContent: "center",
    padding: 10,
    paddingTop: 10,
    paddingBottom: 10,
  },
  row: {
    flex: 1,
    flexDirection: "row",
    alignSelf: "stretch",
    justifyContent: "space-between",
    marginBottom: 10,
  },
  touch: {
    backgroundColor: pallete.dark,
    width: 100,
    height: 100,
    margin: 5,
  },
  imageFile: {    
    width: 100,
    height: 100,    
  },
  buttonFile: {

  },
  reviewRoot: {
    flex: 1,
  },
  reviewContent: {
    flex: 1,
    padding: 15,
    backgroundColor: '#000000CB',
    alignItems: 'center',
    justifyContent: 'center',
  },
  reviewBody: {
    flex: 1,
    padding: 20,
    alignContent: "center",
    justifyContent: "space-between",
    backgroundColor: "#FFFFFF",
    borderRadius: 30,
  },
  reviewTitle: {
  },
  reviewImage: {
    flex: 1,
    marginTop: 20,
    alignSelf: "center",
    alignItems: "stretch",
    width: 256,
    height: 256,
  },
  reviewButtons: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  reviewImagesContent: {
    flex: 1,
    backgroundColor: "blue",
    alignItems: "center",
    alignContent: "center",
    alignSelf: "stretch",
  },
  modalRoot: {
    flex: 1,
  },
  modalContent: {
    flex: 1,
    padding: 15,
    backgroundColor: '#000000CB',
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  modalBody: {
    padding: 20,
    alignContent: "stretch",
    justifyContent: "center",
    backgroundColor: "#FFFFFF",
    borderRadius: 30,
  },
});


Files.propTypes = {
  fnOk: PropTypes.func,
};

export default Files;