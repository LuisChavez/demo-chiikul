import React, { useState, useEffect } from 'react';
import { Text, View, TouchableOpacity, StyleSheet, Modal, Image, ScrollView, PermissionsAndroid } from 'react-native';
import { Camera } from 'expo-camera';
import TextTitle from './TextTitle';
import { pallete } from '../constants';
import ButtonMain from './ButtonMain';
import TextBody from './TextBody';
import Constants from 'expo-constants';
import * as Permissions from 'expo-permissions';
import { Video } from 'expo-av';
import Divider from './Divider';
import { VideoCamera, Icon } from './Icons';


const CameraVideo = ({ handleTakeVideo }) => {

  const [hasPermission, setHasPermission] = useState(null);
  const [showMsg, setShowMsg] = useState(true);
  const [showReview, setShowReview] = useState(false);
  const [video, setVideo] = useState([]);
  const [isRec, setIsRec] = useState(false);

  let instanceCamera = undefined;

  const onRec = async () => {
    setIsRec(true);
    const recod = await instanceCamera.recordAsync({
      maxDuration: 10,      
      mute: false,
    });
    setVideo(recod);
    handleOpenReview();
  }

  const stopRec = () => {
    instanceCamera.stopRecording();
    setIsRec(false);
  }

  const handleRecVideo = () => {
    if (isRec === true) {
      stopRec();
    }
    else {
      onRec();
    }
  }

  const handleCloseMessage = () => {
    setShowMsg(false);
  }

  const handleOpenReview = () => {
    setShowReview(true);
  }

  const handleCloseReview = () => {
    setShowReview(false);
  }

  const handleAcceptVideo = () => {    
    handleTakeVideo(video);
  }

  const handleRepeatVideo = () => {
    handleCloseReview();
  }

  useEffect(() => {
    (async () => {      
      const { status, expires, permissions } = await Permissions.askAsync(
        Permissions.CAMERA,
        Permissions.AUDIO_RECORDING
      );
      setHasPermission(status === 'granted');
      // TODO:  Check available camera. getAvailableCameraTypesAsync is not available on android      

    })();
  }, []);

  if (hasPermission === null) {
    return <View />;
  }

  if (hasPermission === false) {
    return <TextTitle>{'Por favor, permite el uso de la camara'}</TextTitle>
  }

  return (
    <React.Fragment>
      <View style={styles.root}>
        <Camera style={styles.camera} type={Camera.Constants.Type.front}
          ref={(ref) => { instanceCamera = ref; }}>
          <View
            style={styles.viewTop}>
            <TouchableOpacity
              style={styles.cameraButton}
              onPress={handleRecVideo}>
              {isRec === false ?
                <Icon style={styles.icon} round >
                  <VideoCamera width={49}
                    height={49} />
                </Icon> :
                <TextBody textBlue textCenter textMedium
                style={{paddingLeft:15,paddingBottom:5,paddingTop:5,paddingRight:15, borderRadius:10,backgroundColor:"#FFFFFF" }}
                  >{'Detener'}</TextBody>
              }
            </TouchableOpacity>
          </View>
        </Camera>
      </View>
      <Modal style={styles.reviewRoot}
        transparent={true}
        visible={showReview}>
        <View style={styles.reviewContent} >
          <View style={styles.reviewBody} >
            <View style={styles.reviewTitle} >
              <TextBody textBlue >{'Revisión del video.'}</TextBody>
              <Divider />
              <TextBody textParagraph >{'Asegúrate de que el video sea claro y la frase se escuche correctamente.'}</TextBody>
              <TextBody textParagraph textCenter >{'Puedes repetir la grabación si es necesario'}</TextBody>
            </View>
            <View style={styles.reviewVideo} >
              {video.uri &&
                <Video
                  source={{
                    uri: video.uri
                  }}
                  rate={1.0}
                  volume={1.0}
                  isMuted={false}
                  resizeMode="cover"
                  shouldPlay
                  isLooping
                  style={styles.image /*{ width: 300, height: 300 }*/}
                />
              }

            </View>
            <View style={styles.reviewButtons} >
              <ButtonMain
                buttonRed
                buttonPadding
                onPress={handleRepeatVideo}
                text={'Repetir'} />
              <ButtonMain
                buttonBlue
                buttonPadding
                onPress={handleAcceptVideo}
                text={'Continuar'} />
            </View>
          </View>
        </View>
      </Modal>


    </React.Fragment>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
  },
  camera: {
    flex: 1,
  },
  viewTop: {
    flex: 1,
    backgroundColor: 'transparent',
    flexDirection: 'row',
    justifyContent: "center"
  },
  cameraButton: {
    alignSelf: "flex-end",
  },
  icon: {
    borderColor: "#FFFFFF",
    borderStyle: "solid",
    borderWidth: 1,
    width: 64,
    height: 64,
  },
  buttonBlue: {
    backgroundColor: pallete.main,
    alignSelf: "center",
    justifyContent: "center",
    padding: 40,
    paddingTop: 10,
    paddingBottom: 10,
  },
  modalRoot: {
    flex: 1,
  },
  modalContent: {
    flex: 1,
    padding: 15,
    backgroundColor: '#000000CB',
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  modalBody: {
    padding: 20,
    alignContent: "stretch",
    justifyContent: "center",
    backgroundColor: "#FFFFFF",
    borderRadius: 30,
  },

  reviewRoot: {
    flex: 1,
  },
  reviewContent: {
    flex: 1,
    padding: 15,
    backgroundColor: '#000000CB',
    alignItems: 'center',
    justifyContent: 'center',
  },
  reviewBody: {
    flex: 1,
    padding: 20,
    alignContent: "center",
    justifyContent: "space-between",
    backgroundColor: "#FFFFFF",
    borderRadius: 30,
  },
  reviewTitle: {
  },
  reviewVideo: {
    flex: 1,
    justifyContent: "center",
    alignSelf: "stretch",
  },
  reviewButtons: {
    flexDirection: "row",
    justifyContent: "space-evenly",
  },
  image: {
    alignSelf: "stretch",
    flex: 1,
  }


});


CameraVideo.defaultProps = {
  handleTakePicture: () => { },
}

export default CameraVideo;
