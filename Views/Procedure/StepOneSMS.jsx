import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { View, StyleSheet, TextInput } from 'react-native';
import { TextBody, ButtonMain } from '../../Components';
import { routes, pallete } from '../../constants';
import { appStore } from '../../selectors';
import { appActions } from '../../actions/appActions';
import { MainContent } from '../Layouts';
import { Icon, Replay } from '../../Components/Icons';


class StepOneSMS extends Component {

  handleInterval = 0;
  constructor(props) {
    super(props);
    this.state = {
      smsCode: "",
      wait: 60,
    }
  }

  handleChangeTextSMS = (text) => {
    this.setState({ smsCode: text });
  }

  handleValidateSMS = () => {
    const { navigation, confirmSolicitant, infoUser, clearAll } = this.props;
    const { smsCode } = this.state;
    confirmSolicitant(infoUser.id, smsCode).then(result => {
      clearInterval(this.handleInterval);
      navigation.replace(routes.STEP_ONE_TYPE);
    }, (error) => {
      if (error === -1) {//token is not valid
        navigation.popToTop();
      }
    });
  }

  initTimer = () => {
    this.setState({
      wait: 3,
    });
    this.handleInterval = setInterval(() => {
      const { wait } = this.state;
      let r = wait - 1;
      if (r <= 0) {
        clearInterval(this.handleInterval);
      }
      this.setState({ wait: r});
    }, 1000)
  }

  fnSend = () => {
    const { handleInterval } = this.state;
    this.props.sendCode().then(result => {
      this.initTimer();
    });
  }

  componentDidMount() {
    this.props.setIsLoading(false);
    this.initTimer();
  }


  render() {
    const { smsCode, wait } = this.state;

    return (
      <React.Fragment>
        <View style={styles.root} >
          <View style={styles.body}>
            <MainContent>
              <TextBody textCenter textBold style={{ paddingBottom: 30 }} >{'CONFIRMACIÓN DE CONTACTO'}</TextBody>
              <TextBody textParagraph >{'Hemos enviado un codigo de confirmación a tus medios de contacto.'}</TextBody>
              <TextBody textParagraph >{'Escribelo y da click en continuar.'}</TextBody>
              <TextInput
                style={styles.textInput}
                onChangeText={this.handleChangeTextSMS}
                value={smsCode}
              />
              <ButtonMain
                textSmall
                disabled={wait > 0}
                onPress={this.fnSend}
                buttonCenter
                text={`Enviar código de nuevo ${wait > 0 ? 'en '+wait + ' seg.' : ""}`}
                style={styles.btnChange} >
                <Icon round style={{ width: 32, height: 32, backgroundColor: pallete.main }} >
                  <Replay width={17} height={17} />
                </Icon>
              </ButtonMain>
            </MainContent>
          </View>
          <View style={styles.footer}>
            <ButtonMain buttonBlue buttonCenter buttonPadding text={'CONTINUAR'}
              onPress={this.handleValidateSMS}
              disabled={smsCode.length <= 0} />
          </View>
        </View>
      </React.Fragment>
    );
  }
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    padding: 15,
    fontSize: 22,
    backgroundColor: '#f5f5f5',
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  body: {
    flex: 10,
    fontSize: 22,
  },
  footer: {
    flex: 2,
    justifyContent: "center"
  },
  textInput: {
    backgroundColor: "#FFFFFF",
    height: 40,
    borderRadius: 10,
    paddingLeft: 10,
    borderColor: '#000000',
    borderWidth: 1,
    marginTop: 60,
    marginLeft: 30,
    marginRight: 30
  },
  btnChange: {
    flexDirection: "row",
    alignItems: "center",
    paddingTop: 0,
    paddingBottom: 0,
    paddingLeft: 5,
    paddingRight: 5,
  }
});

const mapStateToProps = (state) => ({
  infoUser: appStore.getInfoUser(state),

});

const mapDispathToProps = dispatch => ({
  confirmSolicitant: (id, code) => dispatch(appActions.confirmSolicitant(id, code)),
  clearAll: (payload) => dispatch(appActions.clearAll(payload)),
  setIsLoading: (payload) => dispatch(appActions.setIsLoading(payload)),
  sendCode: () => dispatch(appActions.sendCodes()),

});

StepOneSMS.propTypes = {



};

export default connect(mapStateToProps, mapDispathToProps)(StepOneSMS);