import React from 'react';
import PropTypes from 'prop-types';
import { View, StyleSheet } from 'react-native';
import { pallete } from '../constants';

const Divider = props => {
  return (
    <View style={styles.root} />
  );
};

const styles = StyleSheet.create({
  root: {
    borderColor: pallete.dark,    
    marginTop: 10,
    marginBottom: 10,
    borderBottomWidth:0,
    borderTopWidth:1,
  }
})

Divider.propTypes = {

};

export default Divider;