import React, { useState, useEffect } from 'react';
import { Text, View, TouchableOpacity, StyleSheet, Modal, Image, ScrollView } from 'react-native';
import { Camera } from 'expo-camera';
import TextTitle from './TextTitle';
import { pallete } from '../constants';
import ButtonMain from './ButtonMain';
import TextBody from './TextBody';
import Constants from 'expo-constants';
import Divider from './Divider';
import { Icon, Camera as IconCamera } from './Icons';


const CameraView = ({ handleTakePicture, blockMessage, totalPhotos }) => {

  const [hasPermission, setHasPermission] = useState(null);
  const [showMsg, setShowMsg] = useState(true);
  const [showReview, setShowReview] = useState(false);
  const [countPhoto, setCountPhoto] = useState(0);
  const [photos, setPhotos] = useState([]);
  const [currentPhoto, setCurrentPhoto] = useState({});
  const [showBtn, setShowBtn] = useState(true);

  let instanceCamera = undefined;

  const onCapture = async () => {
    setShowBtn(false);
    const photo = await instanceCamera.takePictureAsync({ base64: false });
    setShowBtn(true);
    setCurrentPhoto(photo);
    handleOpenReview();
  }

  const handleCloseMessage = () => {
    setShowMsg(false);
  }

  const handleOpenReview = () => {
    setShowReview(true);
  }

  const handleCloseReview = () => {
    setShowReview(false);
  }

  const handleAcceptPhoto = () => {
    let p = photos;
    p.push(currentPhoto);
    setPhotos(p);
    if (countPhoto === totalPhotos - 1) {
      handleTakePicture(photos);
    }
    else {
      setCountPhoto(countPhoto + 1);
    }
    handleCloseReview();
  }

  const handleRepeatPhoto = () => {
    handleCloseReview();
  }

  useEffect(() => {
    (async () => {
      const { status } = await Camera.requestPermissionsAsync();
      setHasPermission(status === 'granted');
    })();
  }, []);

  if (hasPermission === null) {
    return <View />;
  }

  if (hasPermission === false) {
    return <TextTitle>{'Por favor, permite el uso de la camara'}</TextTitle>
  }

  return (
    <React.Fragment>
      

      <Modal style={styles.modalRoot}
        transparent={true}
        visible={showMsg}>
        <View style={styles.modalContent} >
          <View style={styles.modalBody} >
            {blockMessage && blockMessage}
            {/* <TextBody style={{ textAlign: "center" }}>{'Al terminar usa el botón siguiente.'}</TextBody> */}
            <ButtonMain
              buttonBlue
              buttonCenter
              buttonPadding
              onPress={handleCloseMessage}
              text={'Entendido'} />
          </View>
        </View>
      </Modal>

      <Modal style={styles.reviewRoot}
        transparent={true}
        visible={showReview}>
        <View style={styles.reviewContent} >
          <View style={styles.reviewBody} >
            <View style={styles.reviewTitle} >
              <TextBody textBlue >{'Revisión de fotografía.'}</TextBody>
              <Divider />
              <TextBody textParagraph >{'Asegúrate que la imagen sea clara y los datos del documento se vean correctamente.'}</TextBody>
              <TextBody textParagraph >{'Puedes repetir la fotografía si es necesario.'}</TextBody>
            </View>
            <View style={styles.reviewImage} >
              <TextBody textMedium textBlue >{`Imagen ${countPhoto + 1} de ${totalPhotos}`}</TextBody>
              {/* <ScrollView style={styles.reviewScroll} > */}
              {currentPhoto.uri &&
                <Image
                  style={styles.image}
                  source={{
                    uri: currentPhoto.uri,
                    isStatic: true
                  }}
                />
              }
              {/* </ScrollView> */}
            </View>
            <View style={styles.reviewButtons} >
              <ButtonMain
                buttonRed
                buttonPadding
                onPress={handleRepeatPhoto}
                text={'Repetir'} />
              <ButtonMain
                buttonBlue
                buttonPadding
                onPress={handleAcceptPhoto}
                text={'Continuar'} />
            </View>
          </View>
        </View>
      </Modal>
      <View style={styles.root}>
        <Camera style={styles.camera} type={Camera.Constants.Type.back}
          ref={(ref) => { instanceCamera = ref; }}>
          <View
            style={styles.viewTop}>
            {showBtn === true &&
              <TouchableOpacity
                style={styles.cameraButton}
                onPress={onCapture}>
                <Icon style={styles.icon} round >
                  <IconCamera width={49}
                    height={49} />
                </Icon>
              </TouchableOpacity>
            }
            {showBtn === false &&
              <TextBody textMedium textCenter style={{color:"#FFFFFF"}} >{'Capturando imagen...'}</TextBody>
            }
          </View>
        </Camera>
      </View>
    </React.Fragment>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
  },
  camera: {
    flex: 1,
  },
  viewTop: {
    flex: 1,
    backgroundColor: 'transparent',
    flexDirection: 'row',
    justifyContent: "center"
  },
  cameraButton: {
    alignSelf: "flex-end",
  },
  icon: {
    borderColor: "#FFFFFF",
    borderStyle: "solid",
    borderWidth: 1,
    width: 64,
    height: 64,
  },
  modalRoot: {
    flex: 1,
  },
  modalContent: {
    flex: 1,
    padding: 15,
    backgroundColor: '#000000CB',
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  modalBody: {
    padding: 20,
    alignContent: "stretch",
    justifyContent: "center",
    backgroundColor: "#FFFFFF",
    borderRadius: 30,
  },

  reviewRoot: {
    flex: 1,
  },
  reviewContent: {
    flex: 1,
    padding: 15,
    backgroundColor: '#000000CB',
    alignItems: 'center',
    justifyContent: 'center',
  },
  reviewBody: {
    flex: 1,
    padding: 20,
    alignContent: "center",
    justifyContent: "space-between",
    backgroundColor: "#FFFFFF",
    borderRadius: 30,
  },
  reviewTitle: {
  },
  reviewImage: {
    flex: 1,
    justifyContent: "center",
    alignSelf: "stretch",
  },
  reviewButtons: {
    flexDirection: "row",
    justifyContent: "space-around",
  },
  image: {
    alignSelf: "stretch",
    flex: 1,
  }


});


CameraView.defaultProps = {
  handleTakePicture: () => { },
  totalPhotos: 1,
}

export default CameraView;
