import React from 'react';
import PropTypes from 'prop-types';
import { Text, StyleSheet } from 'react-native';


const TextTitle = ({ children }) => {
  return (
    <Text style={styles.root}  >
      {children && children}
    </Text>
  );
};

const styles = StyleSheet.create({
  root: {
    fontSize: 32
  },
})

TextTitle.propTypes = {

};

export default TextTitle;