import React from "react";
import Svg, { G, Path } from "react-native-svg";


const Folder = ({ height, width, color, ...style }) => {
  return (
    <Svg
      height={height} width={width} {...style} fill={color}
      viewBox="0 0 64 64"
      xmlns="http://www.w3.org/2000/svg">
      <G id="Tablet-Smartphone-File-Document-Planning">
        <Path d="m23 6h2v2h-2z" />
        <Path d="m27 6h10v2h-10z" />
        <Path d="m34 52h-4a3 3 0 0 0 0 6h4a3 3 0 0 0 0-6zm0 4h-4a1 1 0 0 1 0-2h4a1 1 0 0 1 0 2z" />
        <Path d="m45 26h-1v-11a2 2 0 0 0 -2-2h-20a2 2 0 0 0 -2 2v6h-1a3 3 0 0 0 -3 3v20a3 3 0 0 0 3 3h26a3 3 0 0 0 3-3v-15a3 3 0 0 0 -3-3zm0 2a1 1 0 0 1 1 1v2h-2v-3zm-3-13v16h-9.68l-6.739-4.814a1 1 0 0 0 -.581-.186h-3v-11zm-23 8h1v3h-2v-2a1 1 0 0 1 1-1zm27 21a1 1 0 0 1 -1 1h-26a1 1 0 0 1 -1-1v-16h6.68l6.739 4.814a1 1 0 0 0 .581.186h14z" />
        <Path d="m24 17h2v2h-2z" />
        <Path d="m28 17h4v2h-4z" />
        <Path d="m24 21h16v2h-16z" />
        <Path d="m30 25h6v2h-6z" />
        <Path d="m38 25h2v2h-2z" />
      </G>
    </Svg>
  );
};

Folder.defaultProps = {
  width: 32,
  height: 32,
  color: "#FFFFFF"

}

export default Folder;