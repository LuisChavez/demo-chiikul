import { createAction } from 'redux-actions';
import { appTypeActions } from './../constants';
import { appService } from './../api';
import { setTokenUser, setValidated, tokenIsValid, setStorageUser, getTokenUser } from '../helpers';
import Constants from 'expo-constants';

const IS_DEMO = (() => {
  return Constants.manifest.extra.isDemo || false;
})()

const setSolicitant = createAction(appTypeActions.SET_SOLICITANT, (payload) => (payload));
const setInfoUser = createAction(appTypeActions.SET_INFO_USER, (payload) => (payload));
const saveTokenUser = createAction(appTypeActions.SAVE_TOKEN_USER, (payload) => (setTokenUser(payload)));
const setValidateUser = createAction(appTypeActions.SET_VALIDATED_USER, (payload) => (setValidated(payload)));
const setTaxpayerList = createAction(appTypeActions.SET_TAXPAYER_LIST, (payload) => (payload));
const setTypePerson = createAction(appTypeActions.SET_TYPE_PERSON, (payload) => (payload));
const setPackageRequest = createAction(appTypeActions.SET_PACKAGE_REQUEST, (payload) => (payload));
const setClearStore = createAction(appTypeActions.CLEAR_STORE, (payload) => (payload));
const setIsLoading = createAction(appTypeActions.SET_LOADING, (payload) => (payload));
const setError = createAction(appTypeActions.SET_ERROR, (payload) => (payload));
const setIndexDocument = createAction(appTypeActions.SET_INDEX_DOCUMENT, (payload) => (payload));
const setFolio = createAction(appTypeActions.SET_FOLIO, (payload) => (payload));
const saveStorageUser = createAction(appTypeActions.SAVE_STORAGE_USER, (payload) => (setStorageUser(payload)));

const getErrorDetail = (payload = {}) => {
  let msg = "";
  if (payload.payload !== undefined && payload.payload.detail !== undefined) {
    if (Array.isArray(payload.payload.detail) == true) { mag = payload.payload.detail.reduce((l, c) => (l.concat(c).concat(" "))) }
    else {
      msg = payload.payload.detail;
    }
  }
  else {
    const keys = payload.payload !== undefined ? Object.keys(payload.payload) : [];
    msg = keys.reduce((l, c) => {
      if (c !== "codeError") {
        l = l.concat(c).concat(".- ").concat(payload.payload[c].reduce((_l, _c) => (_l.concat(_c).concat(" ")), ""));
      }
      return l;
    }, "");
  }
  return msg;
}

const registerSolicitant = (payload) => {
  return dispatch => {
    return new Promise((resolve, reject) => {
      dispatch(setIsLoading(true));
      let auxPayload = Object.assign({}, payload);
      let data = Object.assign({}, payload);
      data.telephone = `+52${payload.telephone}`;
      if (IS_DEMO === "true") {
        const data = {
          error: false,
          msg: "",
          payload: { token: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6NywicmZjIjoiQ0FCTDg5MDIwMTdIYSIsImVtYWlsIjoibGNoYXZlemIyMDExQGhvdG1haWwuYnIiLCJ0ZWxlcGhvbmUiOiIrNTI1NTI1MDU3NDE5IiwiZXhwIjoxNTg0OTg4MDcxLjk3NzQzfQ.xevH7SiUhBB8gz-MHHZPzXtoN4ZVMxmMvDPwaPYcrqU" }
        }
        saveTokenUser("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6NywicmZjIjoiQ0FCTDg5MDIwMTdIYSIsImVtYWlsIjoibGNoYXZlemIyMDExQGhvdG1haWwuYnIiLCJ0ZWxlcGhvbmUiOiIrNTI1NTI1MDU3NDE5IiwiZXhwIjoxNTg0OTg4MDcxLjk3NzQzfQ.xevH7SiUhBB8gz-MHHZPzXtoN4ZVMxmMvDPwaPYcrqU")
        dispatch(setIsLoading(false));
        resolve(data.payload);
      } else {
        setTimeout(() => { // it is necessary for ios
          dispatch(appService.registerSolicitant(data))
            .then(({ payload }) => {
              if (payload && payload.error === true) {
                if (payload.status === 500) {
                  dispatch(setError({ show: true, info: payload.msg }))
                }
                else {
                  dispatch(setError({
                    show: true,
                    info: payload.msg.concat(getErrorDetail(payload))
                  }))
                }
                reject(false);
              }
              else {
                dispatch(saveTokenUser(payload.payload.token)).then(result => {
                  getTokenUser().then(tkn => {
                    auxPayload.id = tkn.id;
                    dispatch(saveStorageUser(auxPayload)).then(r => {
                      payload.msg = 'El registro ha sido enviado con éxito.';
                      resolve(payload);
                    });
                  });
                }, error => {
                  dispatch(setError({ show: true, info: payload.msg }))
                  reject({
                    error: true,
                    msg: 'La aplicación no cuenta con permisos suficientes.',
                    payload: data.payload,
                  });
                });
              }
            }, (error) => {
              reject(error);
            })
            .finally(() => {
              dispatch(setIsLoading(false));
            });
        }, 500);

      }

    });
  };
};

const confirmSolicitant = (id, code) => {
  return dispatch => {
    return new Promise((resolve, reject) => {
      dispatch(setIsLoading(true));
      tokenIsValid().then(isValid => {
        if (isValid === true) {
          if (IS_DEMO === "true") {
            dispatch(setValidateUser("true"))
            const data = {};
            data.payload = {};
            data.payload.msg = 'Código de confirmación validado correctamente.';
            dispatch(setIsLoading(false));
            resolve(data.payload);
          } else {
            setTimeout(() => { // it is necessary for ios
              dispatch(appService.confirmSolicitant({ id, code }))
                .then(({ payload }) => {
                  if (payload && payload.error === true) {
                    if (payload.status === 500) {
                      dispatch(setError({ show: true, info: payload.msg }))
                    }
                    if (payload.status === 404) {
                      dispatch(setError({ show: true, info: payload.msg.concat(' Código incorrecto. ') }))
                    }
                    else {
                      dispatch(setError({
                        show: true,
                        info: payload.msg.concat(getErrorDetail(payload))
                      }))
                    }
                    reject(false);
                  }
                  else {
                    dispatch(setValidateUser("true")).then(result => {
                      let data = {};
                      data.payload = {};
                      data.payload.msg = 'Código de confirmación validado correctamente.';
                      resolve(data.payload);
                    }, error => {
                      dispatch(setError({
                        show: true,
                        info: "No fue posible establecer la información como valida",
                      }));
                      reject({
                        error: true,
                        msg: 'Código incorrecto. ' + error,
                        payload: data.payload,
                      });
                    });
                  }
                }, (error) => {
                  reject(error);
                })
                .finally(() => {
                  dispatch(setIsLoading(false));
                });
            }, 500);
          }
        }
        else {
          setTimeout(() => { // it is necessary for ios
            dispatch(setIsLoading(false));
            dispatch(clearAll()).then(ok => {
              dispatch(setError({ show: true, info: "El tiempo para terminar la solicitud ha terminado." }));
              reject(-1);
            });
          }, 500);
        }
      })
    });
  };
};

const fetchTaxpayerList = () => {
  return dispatch => {
    return new Promise((resolve, reject) => {
      dispatch(setIsLoading(true));
      if (IS_DEMO === "true") {
        dispatch(setIsLoading(false));
        dispatch(setTaxpayerList(
          [
            {
              "id": 1,
              "name": "Mayor de edad",
              "general_documents": [
                {
                  "id": 1,
                  "name": "Identidad",
                  "types": [
                    {
                      "id": 1,
                      "name": "Identificación Oficial",
                      "pages": 2,
                      "description": "Identificación Oficial",
                      "category": 1
                    },
                    {
                      "id": 2,
                      "name": "Curp",
                      "pages": 1,
                      "description": "Clave Única de Registro de Población",
                      "category": 1
                    }
                  ]
                },
                {
                  "id": 2,
                  "name": "Solicitud",
                  "types": [
                    {
                      "id": 3,
                      "name": "Solicitud de Contraseña",
                      "pages": 4,
                      "description": "Solicitud de Contraseña",
                      "category": 2
                    },
                    {
                      "id": 4,
                      "name": "Licencia de conducir",
                      "pages": 0,
                      "description": "Licencia de conducir expedida por la Secretaría de Movilidad",
                      "category": 2
                    },
                    {
                      "id": 5,
                      "name": "Cédula profesional",
                      "pages": 1,
                      "description": "Cédula profesional",
                      "category": 2
                    },
                    {
                      "id": 6,
                      "name": "Pasaporte",
                      "pages": 1,
                      "description": "Pasaporte expedido por la Secretaría de Relaciones Exteriores.",
                      "category": 2
                    }
                  ]
                },
                {
                  "id": 3,
                  "name": "Comprobante de domicilio",
                  "types": [
                    {
                      "id": 7,
                      "name": "Estado de cuenta bancario",
                      "pages": 1,
                      "description": "Estado de cuenta bancario a nombre del contribuyente.",
                      "category": 3
                    },
                    {
                      "id": 8,
                      "name": "Recibo de servicio de luz",
                      "pages": 1,
                      "description": "Recibo de servicio de luz",
                      "category": 3
                    },
                    {
                      "id": 9,
                      "name": "Recibo de servicio de gas",
                      "pages": 1,
                      "description": "Recibo de servicio de gas",
                      "category": 3
                    },
                    {
                      "id": 10,
                      "name": "Recibo de servicio de TV de paga",
                      "pages": 1,
                      "description": "Recibo de servicio de TV de paga",
                      "category": 3
                    },
                    {
                      "id": 11,
                      "name": "Recibo de servicio de internet",
                      "pages": 1,
                      "description": "Recibo de servicio de internet",
                      "category": 3
                    },
                    {
                      "id": 12,
                      "name": "Recibo de servicio de teléfono",
                      "pages": 1,
                      "description": "Recibo de servicio de teléfono",
                      "category": 3
                    },
                    {
                      "id": 13,
                      "name": "Recibo de servicio de agua",
                      "pages": 0,
                      "description": "Recibo de servicio de agua",
                      "category": 3
                    }
                  ]
                }
              ],
              "specific_documents": []
            },
            {
              "id": 2,
              "name": "Menor de edad",
              "general_documents": [
                {
                  "id": 1,
                  "name": "Identidad",
                  "types": [
                    {
                      "id": 1,
                      "name": "Acta de nacimiento",
                      "pages": 1,
                      "description": "Acta de nacimiento",
                      "category": 1
                    },
                    {
                      "id": 2,
                      "name": "Curp",
                      "pages": 1,
                      "description": "Clave Única de Registro de Población",
                      "category": 1
                    }
                  ]
                },
                {
                  "id": 2,
                  "name": "Identificación",
                  "types": [
                    {
                      "id": 3,
                      "name": "INE",
                      "pages": 2,
                      "description": "Credencial para votar expedida por el Instituto Nacional Electoral",
                      "category": 2
                    },
                    {
                      "id": 4,
                      "name": "Licencia de conducir",
                      "pages": 0,
                      "description": "Licencia de conducir expedida por la Secretaría de Movilidad",
                      "category": 2
                    },
                    {
                      "id": 5,
                      "name": "Cédula profesional",
                      "pages": 1,
                      "description": "Cédula profesional",
                      "category": 2
                    },
                    {
                      "id": 6,
                      "name": "Pasaporte",
                      "pages": 1,
                      "description": "Pasaporte expedido por la Secretaría de Relaciones Exteriores.",
                      "category": 2
                    }
                  ]
                }
              ],
              "specific_documents": [
                {
                  "id": 14,
                  "name": "Conformidad patria potestad",
                  "pages": 0,
                  "description": "Manifestación por escrito de conformidad de los padres para que uno de ellos actúe como representante del menor. En los casos en que, el acta de nacimiento, la resolución judicial o documento emitido por fedatario público en el que conste la patria potestad se encuentre señalado solamente un padre, no será necesario presentar este requisito.",
                  "category": 4
                }
              ]
            },
            {
              "id": 3,
              "name": "Personas físicas con incapacidad legal judicialmente declarada",
              "general_documents": [
                {
                  "id": 1,
                  "name": "Identidad",
                  "types": [
                    {
                      "id": 1,
                      "name": "Acta de nacimiento",
                      "pages": 1,
                      "description": "Acta de nacimiento",
                      "category": 1
                    },
                    {
                      "id": 2,
                      "name": "Curp",
                      "pages": 1,
                      "description": "Clave Única de Registro de Población",
                      "category": 1
                    }
                  ]
                },
                {
                  "id": 2,
                  "name": "Identificación",
                  "types": [
                    {
                      "id": 3,
                      "name": "INE",
                      "pages": 2,
                      "description": "Credencial para votar expedida por el Instituto Nacional Electoral",
                      "category": 2
                    },
                    {
                      "id": 4,
                      "name": "Licencia de conducir",
                      "pages": 0,
                      "description": "Licencia de conducir expedida por la Secretaría de Movilidad",
                      "category": 2
                    },
                    {
                      "id": 5,
                      "name": "Cédula profesional",
                      "pages": 1,
                      "description": "Cédula profesional",
                      "category": 2
                    },
                    {
                      "id": 6,
                      "name": "Pasaporte",
                      "pages": 1,
                      "description": "Pasaporte expedido por la Secretaría de Relaciones Exteriores.",
                      "category": 2
                    }
                  ]
                },
                {
                  "id": 3,
                  "name": "Comprobante de domicilio",
                  "types": [
                    {
                      "id": 7,
                      "name": "Estado de cuenta bancario",
                      "pages": 1,
                      "description": "Estado de cuenta bancario a nombre del contribuyente.",
                      "category": 3
                    },
                    {
                      "id": 8,
                      "name": "Recibo de servicio de luz",
                      "pages": 1,
                      "description": "Recibo de servicio de luz",
                      "category": 3
                    },
                    {
                      "id": 9,
                      "name": "Recibo de servicio de gas",
                      "pages": 1,
                      "description": "Recibo de servicio de gas",
                      "category": 3
                    },
                    {
                      "id": 10,
                      "name": "Recibo de servicio de TV de paga",
                      "pages": 1,
                      "description": "Recibo de servicio de TV de paga",
                      "category": 3
                    },
                    {
                      "id": 11,
                      "name": "Recibo de servicio de internet",
                      "pages": 1,
                      "description": "Recibo de servicio de internet",
                      "category": 3
                    },
                    {
                      "id": 12,
                      "name": "Recibo de servicio de teléfono",
                      "pages": 1,
                      "description": "Recibo de servicio de teléfono",
                      "category": 3
                    },
                    {
                      "id": 13,
                      "name": "Recibo de servicio de agua",
                      "pages": 0,
                      "description": "Recibo de servicio de agua",
                      "category": 3
                    }
                  ]
                }
              ],
              "specific_documents": [
                {
                  "id": 15,
                  "name": "Resolución incapacidad de contribuyente",
                  "pages": 0,
                  "description": "Original o copia certificada y copia simple de la resolución judicial definitiva, en la cual se declare la incapacidad del contribuyente y contenga la designación como tutor.",
                  "category": 5
                }
              ]
            },
            {
              "id": 4,
              "name": "Contribuyentes en apertura de sucesión",
              "general_documents": [
                {
                  "id": 1,
                  "name": "Identidad",
                  "types": [
                    {
                      "id": 1,
                      "name": "Acta de nacimiento",
                      "pages": 1,
                      "description": "Acta de nacimiento",
                      "category": 1
                    },
                    {
                      "id": 2,
                      "name": "Curp",
                      "pages": 1,
                      "description": "Clave Única de Registro de Población",
                      "category": 1
                    }
                  ]
                },
                {
                  "id": 2,
                  "name": "Identificación",
                  "types": [
                    {
                      "id": 3,
                      "name": "INE",
                      "pages": 2,
                      "description": "Credencial para votar expedida por el Instituto Nacional Electoral",
                      "category": 2
                    },
                    {
                      "id": 4,
                      "name": "Licencia de conducir",
                      "pages": 0,
                      "description": "Licencia de conducir expedida por la Secretaría de Movilidad",
                      "category": 2
                    },
                    {
                      "id": 5,
                      "name": "Cédula profesional",
                      "pages": 1,
                      "description": "Cédula profesional",
                      "category": 2
                    },
                    {
                      "id": 6,
                      "name": "Pasaporte",
                      "pages": 1,
                      "description": "Pasaporte expedido por la Secretaría de Relaciones Exteriores.",
                      "category": 2
                    }
                  ]
                },
                {
                  "id": 3,
                  "name": "Comprobante de domicilio",
                  "types": [
                    {
                      "id": 7,
                      "name": "Estado de cuenta bancario",
                      "pages": 1,
                      "description": "Estado de cuenta bancario a nombre del contribuyente.",
                      "category": 3
                    },
                    {
                      "id": 8,
                      "name": "Recibo de servicio de luz",
                      "pages": 1,
                      "description": "Recibo de servicio de luz",
                      "category": 3
                    },
                    {
                      "id": 9,
                      "name": "Recibo de servicio de gas",
                      "pages": 1,
                      "description": "Recibo de servicio de gas",
                      "category": 3
                    },
                    {
                      "id": 10,
                      "name": "Recibo de servicio de TV de paga",
                      "pages": 1,
                      "description": "Recibo de servicio de TV de paga",
                      "category": 3
                    },
                    {
                      "id": 11,
                      "name": "Recibo de servicio de internet",
                      "pages": 1,
                      "description": "Recibo de servicio de internet",
                      "category": 3
                    },
                    {
                      "id": 12,
                      "name": "Recibo de servicio de teléfono",
                      "pages": 1,
                      "description": "Recibo de servicio de teléfono",
                      "category": 3
                    },
                    {
                      "id": 13,
                      "name": "Recibo de servicio de agua",
                      "pages": 0,
                      "description": "Recibo de servicio de agua",
                      "category": 3
                    }
                  ]
                }
              ],
              "specific_documents": [
                {
                  "id": 16,
                  "name": "Nombramiento y aceptación de albacea",
                  "pages": 0,
                  "description": "Original o copia certificada del documento en donde conste su nombramiento y aceptación del cargo de albacea, otorgado mediante resolución judicial o en documento notarial, según proceda conforme a la legislación de la materia",
                  "category": 6
                }
              ]
            },
            {
              "id": 5,
              "name": "Contribuyentes declarados ausentes",
              "general_documents": [
                {
                  "id": 1,
                  "name": "Identidad",
                  "types": [
                    {
                      "id": 1,
                      "name": "Acta de nacimiento",
                      "pages": 1,
                      "description": "Acta de nacimiento",
                      "category": 1
                    },
                    {
                      "id": 2,
                      "name": "Curp",
                      "pages": 1,
                      "description": "Clave Única de Registro de Población",
                      "category": 1
                    }
                  ]
                },
                {
                  "id": 2,
                  "name": "Identificación",
                  "types": [
                    {
                      "id": 3,
                      "name": "INE",
                      "pages": 2,
                      "description": "Credencial para votar expedida por el Instituto Nacional Electoral",
                      "category": 2
                    },
                    {
                      "id": 4,
                      "name": "Licencia de conducir",
                      "pages": 0,
                      "description": "Licencia de conducir expedida por la Secretaría de Movilidad",
                      "category": 2
                    },
                    {
                      "id": 5,
                      "name": "Cédula profesional",
                      "pages": 1,
                      "description": "Cédula profesional",
                      "category": 2
                    },
                    {
                      "id": 6,
                      "name": "Pasaporte",
                      "pages": 1,
                      "description": "Pasaporte expedido por la Secretaría de Relaciones Exteriores.",
                      "category": 2
                    }
                  ]
                },
                {
                  "id": 3,
                  "name": "Comprobante de domicilio",
                  "types": [
                    {
                      "id": 7,
                      "name": "Estado de cuenta bancario",
                      "pages": 1,
                      "description": "Estado de cuenta bancario a nombre del contribuyente.",
                      "category": 3
                    },
                    {
                      "id": 8,
                      "name": "Recibo de servicio de luz",
                      "pages": 1,
                      "description": "Recibo de servicio de luz",
                      "category": 3
                    },
                    {
                      "id": 9,
                      "name": "Recibo de servicio de gas",
                      "pages": 1,
                      "description": "Recibo de servicio de gas",
                      "category": 3
                    },
                    {
                      "id": 10,
                      "name": "Recibo de servicio de TV de paga",
                      "pages": 1,
                      "description": "Recibo de servicio de TV de paga",
                      "category": 3
                    },
                    {
                      "id": 11,
                      "name": "Recibo de servicio de internet",
                      "pages": 1,
                      "description": "Recibo de servicio de internet",
                      "category": 3
                    },
                    {
                      "id": 12,
                      "name": "Recibo de servicio de teléfono",
                      "pages": 1,
                      "description": "Recibo de servicio de teléfono",
                      "category": 3
                    },
                    {
                      "id": 13,
                      "name": "Recibo de servicio de agua",
                      "pages": 0,
                      "description": "Recibo de servicio de agua",
                      "category": 3
                    }
                  ]
                }
              ],
              "specific_documents": [
                {
                  "id": 17,
                  "name": "Designación representante legal",
                  "pages": 0,
                  "description": "Original de la resolución judicial en la que conste la designación como representante legal y se manifieste la declaratoria especial de ausencia del contribuyente.",
                  "category": 5
                },
                {
                  "id": 18,
                  "name": "Ausencia vigente",
                  "pages": 0,
                  "description": "Manifiesto en el que, bajo protesta de decir verdad, se indique que la situación de ausencia del contribuyente no se ha modificado a la fecha.",
                  "category": 4
                }
              ]
            },
            {
              "id": 6,
              "name": "Contribuyentes privados de su libertad",
              "general_documents": [
                {
                  "id": 1,
                  "name": "Identidad",
                  "types": [
                    {
                      "id": 1,
                      "name": "Acta de nacimiento",
                      "pages": 1,
                      "description": "Acta de nacimiento",
                      "category": 1
                    },
                    {
                      "id": 2,
                      "name": "Curp",
                      "pages": 1,
                      "description": "Clave Única de Registro de Población",
                      "category": 1
                    }
                  ]
                },
                {
                  "id": 2,
                  "name": "Identificación",
                  "types": [
                    {
                      "id": 3,
                      "name": "INE",
                      "pages": 2,
                      "description": "Credencial para votar expedida por el Instituto Nacional Electoral",
                      "category": 2
                    },
                    {
                      "id": 4,
                      "name": "Licencia de conducir",
                      "pages": 0,
                      "description": "Licencia de conducir expedida por la Secretaría de Movilidad",
                      "category": 2
                    },
                    {
                      "id": 5,
                      "name": "Cédula profesional",
                      "pages": 1,
                      "description": "Cédula profesional",
                      "category": 2
                    },
                    {
                      "id": 6,
                      "name": "Pasaporte",
                      "pages": 1,
                      "description": "Pasaporte expedido por la Secretaría de Relaciones Exteriores.",
                      "category": 2
                    }
                  ]
                }
              ],
              "specific_documents": [
                {
                  "id": 19,
                  "name": "Poder general para actos de dominio",
                  "pages": 0,
                  "description": "El representante legal deberá acreditar su calidad con un poder general para actos de dominio o de administración otorgado ante fedatario público.",
                  "category": 7
                },
                {
                  "id": 20,
                  "name": "Resolución privación de libertad",
                  "pages": 0,
                  "description": "Original o copia certificada del acuerdo, auto o resolución que acredite la privación de la libertad del contribuyente y/o orden de arraigo firmada por un Juez.",
                  "category": 5
                }
              ]
            }
          ]))
        dispatch(setIsLoading(false));
        resolve(true);
      } else {
        dispatch(appService.fetchTaxpayerList())
          .then(({ payload }) => {
            if (payload && payload.error === true) {
              dispatch(setError({ show: true, info: payload.msg }))
              reject(false);
            }
            else {
              dispatch(setTaxpayerList(payload.payload));
              resolve(true);
            }
          }, (error) => {
            reject(error);
          })
          .finally(() => {
            dispatch(setIsLoading(false));
          });
      }
    });
  };
};

const sendPackageRequest = (payload) => {
  return dispatch => {
    return new Promise((resolve, reject) => {
      dispatch(setIsLoading(true));
      tokenIsValid().then(isValid => {
        if (isValid === true) {
          let data = Object.assign({}, payload);
          data.type = "generation";
          if (IS_DEMO === "true") {
            dispatch(setIsLoading(false));
            resolve({});
          } else {
            setTimeout(() => {              
              dispatch(appService.sendPackageRequest(data))
                .then(({ payload }) => {
                  if (payload && payload.error === true) {
                    if (payload.status === 500) {
                      dispatch(setError({ show: true, info: payload.msg }))
                    }
                    else { 
                      dispatch(setError({
                        show: true,
                        info: payload.msg.concat(" ").concat(getErrorDetail(payload))
                      }))
                    }
                    reject(false);
                  }
                  else {
                    resolve(payload);
                  }
                }, (error) => {
                  reject(error);
                }).finally(() => {
                  dispatch(setIsLoading(false));
                })
            }, 500);
          }
        }
        else {
          setTimeout(() => { // it is necessary for ios
            dispatch(setIsLoading(false));
            dispatch(clearAll()).then(ok => {
              dispatch(setError({ show: true, info: "El tiempo para terminar la solicitud ha terminado." }));
              reject(-1);
            });
          }, 500);
        }
      })
    });
  };
};

const clearAll = (payload) => {
  return dispatch => {
    return new Promise((resolve, reject) => {
      dispatch(setIsLoading(true));
      dispatch(saveTokenUser(""))
        .then((data) => {
          dispatch(setValidateUser("false"));
          dispatch(setClearStore());
          resolve();
        }, (error) => {
          error.msg = "Sin permisos de almacenamiento";
          reject(error);
        })
        .finally(() => {
          dispatch(setIsLoading(false));
        });
    });
  };
};

const fetchCaptcha = () => {
  return dispatch => {
    return new Promise((resolve, reject) => {
      dispatch(setIsLoading(true));
      if (IS_DEMO === "true") {
        dispatch(setIsLoading(false));
        resolve({ id: 1 });
      } else {
        dispatch(appService.fetchCaptcha())
          .then(({ payload }) => {
            if (payload && payload.error === true) {
              dispatch(setError({ show: true, info: payload.msg }))
              reject(false);
            }
            else {
              resolve(payload.payload);
            }
          }, (error) => {
            reject(error);
          })
          .finally(() => {
            dispatch(setIsLoading(false));
          });
      }
    });
  };
};

const sendCodes = () => {
  return dispatch => {
    return new Promise((resolve, reject) => {
      dispatch(setIsLoading(true));
      tokenIsValid().then(isValid => {
        if (isValid === true) {
          if (IS_DEMO === "true") {                        
            dispatch(setIsLoading(false));
            resolve(true);
          } else {
            setTimeout(() => { // it is necessary for ios
              dispatch(appService.sendCodes({}))
                .then(({ payload }) => {
                  if (payload && payload.error === true) {
                    if (payload.status === 500) {
                      dispatch(setError({ show: true, info: payload.msg }))
                    }                    
                    else {
                      dispatch(setError({
                        show: true,
                        info: payload.msg.concat(getErrorDetail(payload))
                      }))
                    }
                    reject(false);
                  }
                  else {
                    resolve(true);                    
                  }
                }, (error) => {
                  reject(error);
                })
                .finally(() => {
                  dispatch(setIsLoading(false));
                });
            }, 500);
          }
        }
        else {
          setTimeout(() => { // it is necessary for ios
            dispatch(setIsLoading(false));
            dispatch(clearAll()).then(ok => {
              dispatch(setError({ show: true, info: "El tiempo para terminar la solicitud ha terminado." }));
              reject(-1);
            });
          }, 500);
        }
      })
    });
  };
};

/**
 * Actions to handle app
 */
export const appActions = {
  setSolicitant,
  registerSolicitant,
  setInfoUser,
  confirmSolicitant,
  fetchTaxpayerList,
  setTypePerson,
  setPackageRequest,
  sendPackageRequest,
  clearAll,
  setIsLoading,
  fetchCaptcha,
  setError,
  setIndexDocument,
  setFolio,
  sendCodes,
}