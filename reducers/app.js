import { handleActions } from 'redux-actions';
import {
  appTypeActions,
} from './../constants'


const APP_DUMMY = {
  solicitant: {
    rfc: "",
    email: "",
    telephone: "",
    catpcha_id: "",
    catpcha_res: "",
  },
  info_user: {},
  taxpayer_list: [],
  type_person: {},
  package_request: {},
  isLoading: false,
  error: { show: false, info: "" },
  index_document: 0,
  folio: "",
};

export const app = handleActions({
  [appTypeActions.SET_SOLICITANT]: (state, action) => {
    return ({ ...state, solicitant: action.payload })
  },
  [appTypeActions.SET_INFO_USER]: (state, action) => {
    return ({ ...state, info_user: action.payload })
  },
  [appTypeActions.SET_TAXPAYER_LIST]: (state, action) => {
    return ({ ...state, taxpayer_list: action.payload })
  },
  [appTypeActions.SET_TYPE_PERSON]: (state, action) => {
    return ({ ...state, type_person: action.payload })
  },
  [appTypeActions.SET_PACKAGE_REQUEST]: (state, action) => {
    return ({ ...state, package_request: action.payload })
  },
  [appTypeActions.CLEAR_STORE]: (state, action) => {
    let data = Object.assign({}, APP_DUMMY);
    data.error = state.error;
    return ({ ...APP_DUMMY })
  },
  [appTypeActions.SET_LOADING]: (state, action) => {
    return ({ ...state, isLoading: action.payload })
  },
  [appTypeActions.SET_ERROR]: (state, action) => {
    return ({ ...state, error: action.payload })
  },
  [appTypeActions.SET_INDEX_DOCUMENT]: (state, action) => {
    return ({ ...state, index_document: action.payload })
  },
  [appTypeActions.SET_FOLIO]: (state, action) => {
    return ({ ...state, folio: action.payload })
  },
},
  APP_DUMMY
);