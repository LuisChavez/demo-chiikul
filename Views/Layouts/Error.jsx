import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { SafeAreaView, View, StyleSheet, ScrollView } from 'react-native';
import { TextBody, ButtonMain } from '../../Components';
import { pallete } from '../../constants';

class Error extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hasError: false,
      error: {}
    };
  }

  closeError = () => {
    this.setState({ hasError: false, error: {} });
  }

  static getDerivedStateFromError(error) {
    return { hasError: true, error: error };
  }

  componentDidCatch(error, errorInfo) {
    // TODO send error
    //logErrorToMyService(error, errorInfo);
  }

  render() {
    if (this.state.hasError) {
      return (
        <SafeAreaView style={styles.container}>
          <ScrollView>
            <View style={styles.content}>
              <TextBody textParagraph textTitle textBlue textCenter >{'¡Oops!'}</TextBody>
              <TextBody textParagraph >{'Algo no salió bien.'}</TextBody>
              <View style={styles.contentError}>
                <TextBody>{this.state.error.toString()}</TextBody>
              </View>
              <ButtonMain style={styles.btnError} buttonBlue buttonCenter buttonBig text={"CERRAR"}
                onPress={this.closeError}
              />
            </View>
          </ScrollView>
        </SafeAreaView>
      );
    }
    else {
      return this.props.children;
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 30,
    paddingBottom: 15,
    paddingLeft: 15,
    paddingRight: 15,
    backgroundColor: pallete.back,
  },
  content: {
    flex: 1,

  },
  contentError: {
    backgroundColor: "#FFFFFF",
    borderRadius: 10,
    paddingBottom: 15,
    paddingRight: 15,
    paddingLeft: 15,
    paddingTop: 15,
    marginBottom: 15,
  },
  btnError: {
    alignSelf: "flex-end",
  }

})

Error.propTypes = {

};

export default Error;