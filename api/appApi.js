import { createAction } from 'redux-actions';
import { appTypeActions, url_api } from '../constants';
import { apiPost, apiGet, apiPostForm } from './core';

const registerSolicitant = createAction(appTypeActions.REGISTER_SOLICITANT,
  (payload) => apiPost(url_api.solicitant.resource, payload, true)
    .then(
      data => {
        return {
          error: false,
          msg: "",
          payload: data
        };
      },
      error => {
        if (error.codeError === undefined) // Error on server
        {
          return {
            error: true,
            msg: `No fue posible registrar al solicitante. ${error.status === 500 ? 'Error en el servidor.' : 'Verifica que tengas conexión a internet.'}`,
            rawError: error,
            status: error.status,
          };
        }
        else //error 
        {
          return {
            error: true,
            status: error.status,
            msg: 'No fue posible realizar el registro. ',
            payload: error,
          };
        }
      },
    )
);

const confirmSolicitant = createAction(appTypeActions.CONFIRM_SOLICITANT,
  (payload) => apiPost(url_api.solicitant.sendValidation.replace('{id}', payload.id), payload, false)
    .then(
      data => {
        return {
          error: false,
          msg: "",
          payload: data
        };
      },
      error => {
        if (error.codeError === undefined) // Error server
        {
          return {
            error: true,
            msg: `No fue posible confirmar al solicitante. ${error.status === 500 ? 'Error en el servidor.' : ''}`,
            rawError: error,
            status: error.status,
          };
        }
        else //error 
        {//404  no user
          return {
            error: true,
            msg: 'No fue posible procesar la solicitud.',
            payload: error,
          };
        }
      },
    )
);

const fetchTaxpayerList = createAction(appTypeActions.FETCH_TAXPAYER_LIST,
  (payload) => apiGet(url_api.catalog.taxpayerTypes.resource)
    .then(
      data => {
        return {
          error: false,
          msg: "",
          payload: data
        };
      },
      error => {
        if (error.error === undefined) // Error server
        {
          return ({
            error: true,
            msg: 'No fue posible obtener el listado de tramites. Verifica que tengas conexión a internet.'
          });
        }
        else //error 
        {
          return {
            error: true,
            msg: 'No fue posible procesar la solicitud.',
            payload: error,
          };
        }
      },
    )
);

const sendPackageRequest = createAction(appTypeActions.SEND_PACKAGE_REQUEST,
  (payload) => apiPostForm(url_api.solicitant.sendSolicitude, payload)
    .then(
      data => {
        return {
          error: false,
          msg: "",
          payload: data
        };
      },
      error => {
        if (error.codeError === undefined) // Error server
        {
          return {
            error: true,
            msg: `No fue posible registrar al solicitante. ${error.status === 500 ? 'Error en el servidor.': ''}`,
            rawError: error,
          };
        }
        else //error
        {
          return {
            error: true,
            msg: 'No fue posible procesar la solicitud.',
            payload: error,
          };
        }
      },
    )
);

const fetchCaptcha = createAction(appTypeActions.FETCH_CAPTCHA,
  (payload) => apiGet(url_api.captcha.resource)
    .then(
      data => {
        return {
          error: false,
          msg: "",
          payload: data
        };
      },
      error => {
        if (error.error === undefined) // Error server
        {
          return {
            error: true,
            msg: 'No fue posible obtener la imagen captcha. Verifica que tengas conexión a internet.'
          };
        }
        else //error
        {
          return {
            error: true,
            msg: 'No fue posible procesar la solicitud.',
            payload: error,
          };
        }
      },
    )
);

const sendCodes = createAction(appTypeActions.SEND_CODES,
  (payload) => apiPost(url_api.smsService.resource, payload, false)
    .then(
      data => {
        return {
          error: false,
          msg: "",
          payload: data
        };
      },
      error => {
        if (error.codeError === undefined) // Error server
        {
          return {
            error: true,
            msg: `No fue posible confirmar al solicitante. ${error.status === 500 ? 'Error en el servidor.' : error.status === 404? 'No se encontró el servicio.': ''}`,
            rawError: error,
            status: error.status,
          };
        }
        else //error 
        {
          return {
            error: true,
            msg: 'No fue posible procesar la solicitud.',
            payload: error,
          };
        }
      },
    )
);

export const appService = {
  registerSolicitant,
  confirmSolicitant,
  fetchTaxpayerList,
  sendPackageRequest,
  fetchCaptcha,
  sendCodes,
};