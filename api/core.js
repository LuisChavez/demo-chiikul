import { authHeader } from './../helpers';

const getParams = (params) => {
    let formData = new FormData();
    const keys = Object.keys(params);
    let i = 0;
    for (i = 0; i < keys.length; i++) {
        if (Array.isArray(params[keys[i]])) {
            let j = 0;
            for (j = 0; j < params[keys[i]].length; j++) {
                formData.append(keys[i], params[keys[i]][j]);
            }
        }
        else {
            formData.append(keys[i], params[keys[i]]);
        }
    } 
    return formData;
}

export const apiGet = (url) => fetch(url, {
    method: 'GET',
    headers: Object.assign({ 'Content-Type': 'application/json' }, authHeader()),
}).then(
    v => {
        try {
            if (v.ok) { return v.json(); }
            else {
                return v.json().then(
                    result => Promise.reject(Object.assign(result, { codeError: v.status })),
                    (error) => Promise.reject(v));
            };
        }
        catch (Exception) {
            return Promise.reject(v);
        }
    }
);

export const apiPost = async (url, params, publicAction = false) => {
    const v = await fetch(url, {
        method: 'POST',
        headers: Object.assign({ 'Content-Type': 'application/json' }, (publicAction === true ? {} : await authHeader())),
        body: JSON.stringify(params),
    });
    try {
        if (v.ok) {
            if (v.headers.get("Content-Length") > 0) {
                return v.json();
            }
            else {
                return Promise.resolve({});
            }

        }
        else {
            return v.json().then(result => Promise.reject(Object.assign(result, { codeError: v.status })), (error) => Promise.reject(v));
        }
    }
    catch (Exception) {
        return Promise.reject(v);
    }
};

export const apiPostForm = async (url, params) => fetch(url, {
    method: 'POST',
headers: Object.assign({ 'Content-Type': 'multipart/form-data' }, await authHeader()),
    body: getParams(params),
}).then(v => {
    try {
        if (v.ok) {
            if (v.headers.get("Content-Length") > 0) {
                return v.json();
            }
            else {
                return Promise.resolve({});
            }
        }
        else {
            return v.json().then(
                result => Promise.reject(Object.assign(result, { codeError: v.status })),
                (error) => Promise.reject(v));
        };
    }
    catch (Exception) {
        return Promise.reject(v);
    }
})

export const apiPatch = (url, params) => fetch(url, {
    method: 'PATCH',
    headers: Object.assign({ 'Content-Type': 'application/json' }, authHeader()),
    body: JSON.stringify(params),
}).then(v => {
    try {
        if (v.ok) { return v.json(); }
        else {
            return v.json().then(
                result => Promise.reject(Object.assign(result, { codeError: v.status })),
                (error) => Promise.reject(v));
        };
    }
    catch (Exception) {
        return Promise.reject(v);
    }
});

export const apiGetDocument = (url) => fetch(url, {
    method: 'GET',
    headers: Object.assign({ 'Content-Type': 'application/pdf' }, authHeader()),
}).then(v => (v.ok ? v.blob() : { error: true, detail: "No fue posible obtener el documento" }));