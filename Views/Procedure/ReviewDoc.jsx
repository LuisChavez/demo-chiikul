import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Modal, View, StyleSheet } from 'react-native';
import RadioForm from 'react-native-simple-radio-button';
import {
  TextBody,
  ButtonMain,
} from '../../Components';
import { pallete } from '../../constants';

const generateOptions = (list) => {
  return list.reduce((last, current) => {
    last.push({
      value: current,
      label: current.name
    });
    return last;
  }, []);
}

const ReviewDoc = ({ show, fnOk, fnCancel, listDocuments, nameDocument, route }) => {

  const [showMsg, setShowMsg] = useState(show);
  const [typeDocument, setTypeDocument] = useState({});
  const [listTypeDocument, setListTypeDocument] = useState(generateOptions(listDocuments));

  const handleAccept = () => {
    //fnOk(typeDocument);
    fnOk(listTypeDocument[0].value)
  }

  const handleCancel = () => {
    fnCancel();
  }

  const handleSelectType = (value, index) => {
    setTypeDocument(value);
  }

  useEffect(() => {
    setShowMsg(show);
  }, [show]);

  useEffect(() => {
    setListTypeDocument(generateOptions(listDocuments));
  }, [listDocuments]);

  return (
    <Modal style={styles.modal}
      animationType="fade"
      transparent={true}
      visible={showMsg}>
      <View style={styles.root} >
        <View style={styles.body} >
          <TextBody style={styles.msj} textParagraph textBold >
            {"Selecciona la imagen del documento "}
            {nameDocument && <TextBody textBold >
              {nameDocument}{" "}
            </TextBody>}
            <TextBody style={styles.msj} textParagraph textBold >
              {"a adjuntar y al termino pulsa el botón Continuar"}
            </TextBody>

            {/* <TextBody textBold >
              {nameDocument}
            </TextBody>
            {" que adjuntarás."} */}
          </TextBody>
          {/* <RadioForm
            radio_props={listTypeDocument}
            initial={-1}
            formHorizontal={false}
            labelHorizontal={true}
            buttonColor={pallete.main}
            animation={true}
            onPress={handleSelectType}
          /> */}
          <View style={styles.contentButtons} >
            {/* <ButtonMain
              style={styles.button}
              buttonBlue
              onPress={handleCancel}
              text={'Cancelar'} /> */}
            <ButtonMain
              style={styles.button}
              buttonBlue
              onPress={handleAccept}
              text={'Entendido'}
              //disabled={Object.keys(typeDocument).length === 0}
              disable={listTypeDocument[0] === undefined}
            />
          </View>
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  modal: {
    flex: 1,
  },
  root: {
    flex: 1,
    padding: 15,
    backgroundColor: '#000000CB',
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  body: {
    padding: 20,
    alignContent: "center",
    justifyContent: "center",
    backgroundColor: "#FFFFFF",
    borderRadius: 30,
  },
  button: {
    marginTop: 10,
    marginBottom: 10,
    marginLeft: 10,
    marginRight: 10,
  },
  msj: {
    textAlign: "center"
  },
  contentButtons: {
    marginTop: 15,
    flexDirection: "row",
    justifyContent: "center",
  }
});

ReviewDoc.defaultProps = {
  show: false,
  fnOk: () => { },
  fnCancel: () => { },
  listDocuments: [],
  nameDocument: "",
}

ReviewDoc.propTypes = {
  show: PropTypes.bool,
  fnOk: PropTypes.func,
  fnCancel: PropTypes.func,
  listDocuments: PropTypes.array,
  nameDocument: PropTypes.string,
};

export default ReviewDoc;