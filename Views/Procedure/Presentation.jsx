import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, StyleSheet, ScrollView } from 'react-native';
import { TextBody, ButtonMain } from '../../Components';
import { pallete, routes } from '../../constants';
import MainContent from '../Layouts/MainContent';
import {
  Paper,
  User,
  Security,  
  Icon,

} from '../../Components/Icons';
import { connect } from 'react-redux';
import { appActions } from '../../actions/appActions';


const Presentation = ({ navigation,fetchTaxpayerList }) => {

  const handleNext = () => {
    fetchTaxpayerList().then(result=>{
      navigation.replace(routes.PRESENTATION_NEXT);
    });
  }
  return (
    <MainContent>
      <View style={styles.root} >
        <View style={styles.head}>
          <TextBody textParagraph textCenter textBold >
            {'Para poder completar el proceso de Generación o Actualización de Contraseña deberás seguir los siguientes 3 pasos:'}
          </TextBody>
        </View>
        <View style={styles.body}>
          <TextBody style={styles.textCenter}>{'PASO 1'}</TextBody>
          <View style={styles.stepContent} >
            <Icon style={styles.icon} round >
              <User
                width={49}
                height={49}
              />
            </Icon>
            <ScrollView horizontal={true} >
              <View>
                <TextBody textSmall style={styles.stepTitle} >
                  {"Ingresa:"}
                </TextBody>
                <TextBody textSmall style={styles.stepTitle} >
                  {"∘ RFC a 13 posiciones"}
                </TextBody>
                <TextBody textSmall style={styles.stepTitle} >
                  {"∘ Correo electrónico personal"}
                </TextBody>
                <TextBody textSmall style={styles.stepTitle} >
                  {"∘ Número celular a 10 posiciones"}
                </TextBody>
              </View>
            </ScrollView>
          </View>
          <TextBody style={styles.textCenter}>{'PASO 2'}</TextBody>
          <View style={styles.stepContent} >
            <Icon style={styles.icon} round >
              <Paper
                width={49}
                height={49}
              />
            </Icon>
            <ScrollView horizontal={true} >
              <View>
                <TextBody textSmall style={styles.stepTitle} >
                  {"Ingresa la documentación requerida "}
                </TextBody>
                <TextBody textSmall style={styles.stepTitle} >
                  {"por la autoridad:"}
                </TextBody>
                <TextBody textSmall style={styles.stepTitle} >
                  {"∘ Identificación Oficial"}
                </TextBody>
                {/* <TextBody textSmall style={styles.stepTitle} >
                  {"∘ Solicitud de Contraseña"}
                </TextBody> */}
              </View>
            </ScrollView>
          </View>
          <TextBody style={styles.textCenter}>{'PASO 3'}</TextBody>
          <View style={styles.stepContent} >
            <Icon style={styles.icon} round >
              <Security
                width={49}
                height={49}
              />
            </Icon>
            <ScrollView horizontal={true} >
              <View>
                <TextBody textSmall style={styles.stepTitle} >
                  {"∘ Confirma tu identidad a través de"}
                </TextBody>
                <TextBody textSmall style={styles.stepTitle} >
                  {"un video"}
                </TextBody>
                <TextBody textSmall style={styles.stepTitle} >
                  {"∘ Firma la solicitud"}
                </TextBody>
              </View>
            </ScrollView>
          </View>
        </View>
        <View style={styles.footer}>
          <ButtonMain
            buttonBlue
            buttonPadding
            buttonCenter
            text={'EMPEZAR'}
            onPress={handleNext}
          />
        </View>
      </View>
    </MainContent>
  );
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    padding: 15,
    fontSize: 22,
    backgroundColor: '#f5f5f5',
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  head: {
    flex: 2,
    fontSize: 22,
  },
  body: {
    flex: 8,
  },
  footer: {
    flex: 2,
    justifyContent: "center"
  },
  icon: {
    backgroundColor: pallete.main,
    width: 64,
    height: 64,
  },
  stepContent: {
    flexDirection: "row",
    backgroundColor: "#FFFFFF",
    justifyContent: "flex-start",
    alignItems: "center",
    marginTop: 15,
    marginBottom: 15,
    borderRadius: 80,
    paddingRight: 30,

  },
  stepTitle: {
    justifyContent: "center",
  },
  buttonBlue: {
    backgroundColor: pallete.main,
    alignSelf: "center",
    justifyContent: "center",
    padding: 40,
    paddingTop: 10,
    paddingBottom: 10,
  },
  textCenter: {
    alignSelf: "center",
    fontWeight: "bold"
  },

});


const mapDispathToProps = dispatch => ({
  fetchTaxpayerList: (payload) => dispatch(appActions.fetchTaxpayerList()),
});

Presentation.propTypes = {
};

export default connect(null, mapDispathToProps)(Presentation);