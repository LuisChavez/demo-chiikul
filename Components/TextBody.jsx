import React from 'react';
import PropTypes from 'prop-types';
import { Text, StyleSheet } from 'react-native';
import { pallete } from '../constants';


const TextBody = ({ children, textBlue, textRed, textTitle, textBold, textSubline, textParagraph, textMedium, textCenter, textSmall,textJustify, style, ...props }) => {
  return (
    <Text style={Object.assign({}, styles.root,
      textBlue && styles.textBlue,
      textRed && styles.textRed,
      textBold && styles.textBold,
      textTitle && styles.textTitle,
      textMedium && styles.textMedium,
      textSubline && styles.textSubline,
      textParagraph && styles.textParagraph,
      textCenter && styles.textCenter,
      textSmall && styles.textSmall,
      textJustify && styles.textJustify,
      style)}  {...props}  >
      {children && children}
    </Text>
  );
};

const styles = StyleSheet.create({
  root: {
    fontSize: 22,
    color: pallete.fontColor,
    marginBottom: 5,
  },
  textBlue: {
    color: pallete.main,
  },
  textRed: {
    color: pallete.second,
  },
  textTitle: {
    fontSize: 32,
  },
  textMedium: {
    fontSize: 18,
  },
  textSmall: {
    fontSize: 14,
  },
  textBold: {
    fontWeight: "bold",
  },
  textSubline: {
    textDecorationLine: "underline",
  },
  textParagraph: {
    marginBottom: 20,
  },
  textCenter: {
    textAlign: "center",
    alignSelf: "center"
  },
  textJustify: {
    textAlign: "justify",    
  }
})

TextBody.defaultProps = {
  style: {}
}

TextBody.propTypes = {

};

export default TextBody;