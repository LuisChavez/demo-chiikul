import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View } from 'react-native';

const Icon = ({ style, children, width, height, round, ...props }) => {

  return (
    <View style={Object.assign({},
      styles.root,
      round && { borderRadius: width / 2 },
      { width: width, height: height },
      style,
    )}
      {...props} >
      {children && children}
    </View>
  );
};

const styles = StyleSheet.create({
  root: {
    resizeMode: "contain",
    justifyContent: 'center',
    alignItems: 'center',
    margin: 10,
  },
})

Icon.defaultProps = {
  width: 69,
  height: 69,
}

Icon.propTypes = {
  width: PropTypes.number,
  height: PropTypes.number,
  round: PropTypes.bool,
};

export default Icon;