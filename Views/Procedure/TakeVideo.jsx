import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { routes } from '../../constants';
import { CameraVideo } from '../../Components';
import { connect } from 'react-redux';
import { appStore } from '../../selectors';
import { appActions } from '../../actions/appActions';

class TakeVideo extends Component {

  handleVideoOk = (file) => {
    const { setPackageRequest, packageRequest } = this.props;
    let p = packageRequest;
    p["video"] = {
      uri: file.uri,
      type: "video/*",
      name: "video_identity" + file.uri.substring(file.uri.lastIndexOf("."))
    };
    setPackageRequest(Object.assign({}, p));
    this.props.navigation.replace(routes.REVIEW_INFO);
  }

  render() {
    return (
      <CameraVideo                        
        handleTakeVideo={this.handleVideoOk}
      />
    );
  }
}

const mapStateToProps = (state) => ({
  packageRequest: appStore.getPackageRequest(state),
});

const mapDispathToProps = dispatch => ({
  setPackageRequest: (payload) => dispatch(appActions.setPackageRequest(payload))
});

TakeVideo.propTypes = {

};

export default connect(mapStateToProps, mapDispathToProps)(TakeVideo);