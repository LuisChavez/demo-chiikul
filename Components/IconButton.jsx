import React from 'react';
import PropTypes from 'prop-types';
import { TouchableHighlight, Text, StyleSheet, Image, View } from 'react-native';
import { Paper } from './Icons';
import { pallete } from '../constants';

const IconButton = ({ text, img, ...props }) => {
  return (
    <TouchableHighlight style={styles.root} {...props} >
      <React.Fragment>
        <View style={styles.containImage}>
          <Image style={styles.image} source={img} />
          {/* <Paper style={styles.image} width={100} height={100} /> */}
        </View>
        <Text style={styles.text} >
          {text && text}
        </Text>
      </React.Fragment>
    </TouchableHighlight>
  );
};

const styles = StyleSheet.create({
  root: {
    backgroundColor: '#FFFFFF',
    shadowColor: '#29000000',
    shadowRadius: 5,
    padding: 10,
    marginTop: 20,
    marginBottom: 20,
    borderRadius: 100,
    flex: 1,
    flexDirection: "row",
  },
  button: {
    flex: 1,
    backgroundColor: '#000',
    flexDirection: 'column',
    alignSelf: 'stretch',
  },
  text: {
    color: '#625C5C',
    fontSize: 16,
    paddingLeft: 15,
    paddingRight: 15,
    textAlign: "left",
    flex: 7,
    alignSelf: "center",
  },
  containImage: {
    backgroundColor: pallete.main,
    borderRadius: 70 / 2,
    flex: 1.7,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 5,
  },
  image: {
    padding: 0,
    width: 65,
    height: 65,
    borderRadius: 65 / 2,
  }
})

IconButton.propTypes = {

};

export default IconButton;