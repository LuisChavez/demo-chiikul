export { default as MainContent } from './MainContent';
export { default as HeaderNavigation } from './HeaderNavigation';
export { default as Loading } from './Loading';
export { default as Error } from './Error';
export { default as ModalMsg } from './ModalMsg';
