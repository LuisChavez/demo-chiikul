import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, StyleSheet } from 'react-native';
import { TextBody, ButtonMain } from '../../Components';
import { pallete, routes } from '../../constants';
import { connect } from 'react-redux';
import { appStore } from '../../selectors';
import { appActions } from '../../actions/appActions';


class StepOneType extends Component {

  handleNext = () => {
    this.props.navigation.replace(routes.TAKE_VIDEO);
  }

  render() {
    return (
      <View style={styles.root} >
        <View style={styles.body}>
          <TextBody textBold textCenter >{'Verifica tu identidad a través de un video.'}</TextBody>
          <TextBody textCenter style={{ marginBottom: 15, marginTop: 30 }} >
            {'Durante la grabación del video deberás mencionar claramente la siguiente frase:'}</TextBody>

          <View style={styles.content}>
            <TextBody textCenter textBold  >{'“No estudio para saber más, sino para ignorar menos”'}</TextBody>
          </View>

        </View>
        <View style={styles.footer}>
          <ButtonMain buttonBlue buttonCenter buttonPadding text={'GRABAR'} onPress={this.handleNext} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    padding: 15,
    fontSize: 22,
    backgroundColor: '#f5f5f5',
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  body: {
    flex: 10,
    fontSize: 22,
    paddingTop: 15,
  },
  footer: {
    flex: 2,
    justifyContent: "center"
  },
  content: {
    backgroundColor: "#FFFFFF",
    borderRadius: 15,
    paddingBottom: 15,
    paddingRight: 15,
    paddingLeft: 15,
    paddingTop: 15,
    marginTop: 45,
  },
  textInput: {
    backgroundColor: "#FFFFFF",
    height: 40,
    borderRadius: 10,
    paddingLeft: 10,
    borderColor: 'gray', borderWidth: 1
  },
  title: {
    color: pallete.main,
  }
});

const mapStateToProps = (state) => ({
  packageRequest: appStore.getPackageRequest(state),
});

const mapDispathToProps = dispatch => ({
  setPackageRequest: (payload) => dispatch(appActions.setPackageRequest(payload))
});

StepOneType.propTypes = {

};

export default connect(mapStateToProps, mapDispathToProps)(StepOneType);