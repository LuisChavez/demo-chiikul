import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, StyleSheet } from 'react-native';
import { TextBody, ButtonMain, Divider } from '../../Components';
import { pallete, routes } from '../../constants';
import RadioForm from 'react-native-simple-radio-button';
import { connect } from 'react-redux';
import { appStore } from '../../selectors';
import { appActions } from '../../actions/appActions';


class StepOneType extends Component {

  constructor(props) {
    super(props);
    this.state = {
      typePerson: {}
    }
  }

  handleSelectType = (value, index) => {
    this.setState({ typePerson: value });
  }

  handleNext = () => {
    const { navigation, setTypePerson, setIndexDocument } = this.props;
    const { typePerson } = this.state;
    setTypePerson(typePerson);
    setIndexDocument(0);
    navigation.navigate(routes.STEP_TWO);
  }

  componentDidMount() {
    const { navigation, setTypePerson, setIndexDocument, taxpayerOptions } = this.props;
    if (taxpayerOptions.length === 1) {
      setTypePerson(taxpayerOptions[0].value);
      setIndexDocument(0);
      setTimeout(() => {
        navigation.replace(routes.STEP_TWO);
      }, 100);      
    }
  }


  render() {
    const { taxpayerOptions } = this.props;
    const { typePerson } = this.state;

    return (
      taxpayerOptions.length > 1 ?
      <View style={styles.root} >
        <View style={styles.body}>
          <TextBody style={styles.title} >{'Selecciona el tipo de solicitud que deseas realizar.'}</TextBody>
          <Divider />
          <TextBody style={{ marginBottom: 15, marginTop: 15 }} >{'Personas físicas.'}</TextBody>
          <RadioForm
            radio_props={taxpayerOptions}
            initial={null}
            formHorizontal={false}
            labelHorizontal={true}
            buttonColor={pallete.main}
            animation={true}
            onPress={this.handleSelectType}
          />
        </View>
        <View style={styles.footer}>
          <ButtonMain
            buttonBlue
            buttonCenter
            buttonPadding
            text={'Continuar'} onPress={this.handleNext} disabled={Object.keys(typePerson).length === 0}
          />
        </View>
      </View>:null
    );
  }
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    padding: 15,
    fontSize: 22,
    backgroundColor: '#f5f5f5',
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  body: {
    flex: 10,
    fontSize: 22,
  },
  footer: {
    flex: 2,
    justifyContent: "center"
  },
  textInput: {
    backgroundColor: "#FFFFFF",
    height: 40,
    borderRadius: 10,
    paddingLeft: 10,
    borderColor: 'gray', borderWidth: 1
  },
  title: {
    color: pallete.main,
  }
});

const mapStateToProps = (state) => ({
  taxpayerOptions: appStore.taxpayerOptions(state),
});

const mapDispathToProps = dispatch => ({
  setTypePerson: (payload) => dispatch(appActions.setTypePerson(payload)),
  setIndexDocument: (payload) => dispatch(appActions.setIndexDocument(payload)),
});

StepOneType.propTypes = {

};

export default connect(mapStateToProps, mapDispathToProps)(StepOneType);