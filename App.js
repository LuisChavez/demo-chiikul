import React from 'react';
import { StyleSheet } from 'react-native';
import { MainMenu } from './Views';
import {
  Presentation,
  PresentationNext,
  StepOne,
  StepOneSMS,
  StepOneType,
  StepTwo,
  Identity,
  ReviewInfo,
  EndProcedure,
  TakeVideo,
  TakePhoto,
  SelectFiles,
  Sign,
  FeedBack,
} from './Views/Procedure';
import { routes } from './constants';
import { Documentation } from './Views/Review';
import { Provider } from 'react-redux';
import { store } from './store';
import { NavigationContainer, } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { HeaderNavigation, Loading, Error, ModalMsg } from './Views/Layouts';


const Stack = createStackNavigator();

const AppMain = () => {

  return (
    <Provider store={store} >
      <Loading />
      <ModalMsg />
      <Error>
        <NavigationContainer>
          <Stack.Navigator initialRouteName={routes.MAIN_MENU} screenOptions={{ header: (props) => (<HeaderNavigation {...props} />) }}  >
            <Stack.Screen name={routes.MAIN_MENU} component={MainMenu}
              options={{ title: 'Bienvenido' }}
            />
            <Stack.Screen name={routes.PRESENTATION} component={Presentation}
              options={{ title: 'Bienvenido' }} />
            <Stack.Screen name={routes.PRESENTATION_NEXT} component={PresentationNext}
              options={{ title: 'Bienvenido' }} />
            <Stack.Screen name={routes.STEP_ONE} component={StepOne}
              initialParams={{ typeIcon: "User" }}
              options={{ title: 'Paso 1' }} />
            <Stack.Screen name={routes.STEP_ONE_SMS} component={StepOneSMS}
              initialParams={{ typeIcon: "User" }}
              options={{ title: 'Paso 1' }} />
            <Stack.Screen name={routes.STEP_ONE_TYPE} component={StepOneType}
              initialParams={{ typeIcon: "User" }}
              options={{ title: 'Paso 1' }} />
            <Stack.Screen name={routes.STEP_TWO} component={StepTwo}
              initialParams={{ typeIcon: "Paper" }}
              options={{ title: 'Paso 2' }} />

            <Stack.Screen name={routes.CAMERA_VIEW} component={TakePhoto}
              initialParams={{ typeIcon: "Paper" }}
              options={{
                title: 'Paso 2',
                headerShown: false
              }} />

            <Stack.Screen name={routes.IDENTITY} component={Identity}
              initialParams={{ typeIcon: "Security" }}
              options={{
                title: 'Paso 3',
              }} />
            <Stack.Screen name={routes.TAKE_VIDEO} component={TakeVideo}
              initialParams={{ typeIcon: "Security" }}
              options={{
                title: 'Paso 3',
                headerShown: false,
              }} />
            <Stack.Screen name={routes.REVIEW_INFO} component={ReviewInfo}
              initialParams={{ typeIcon: "Folder" }}
              options={{ title: 'Resumen' }} />
            <Stack.Screen name={routes.END_PROCEDURE} component={EndProcedure}
              initialParams={{ typeIcon: "Folder" }}
              options={{ title: 'Solicitud' }} />


            <Stack.Screen name={routes.FILES} component={SelectFiles}
              initialParams={{ typeIcon: "Paper" }}
              options={{ title: 'Paso 2' }} />


            <Stack.Screen name={routes.DOCUMENTATION} component={Documentation}
              initialParams={{ typeIcon: "Paper" }}
              options={{ title: 'Requisitos' }} />

            <Stack.Screen name={routes.SING} component={Sign}
              options={{ title: 'Firma' }}
            />

            <Stack.Screen name={routes.FEED_BACK} component={FeedBack}
              options={{ title: 'Encuesta' }}
            />
          </Stack.Navigator>
        </NavigationContainer>
      </Error>
    </Provider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 15,
    fontSize: 22,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default AppMain;