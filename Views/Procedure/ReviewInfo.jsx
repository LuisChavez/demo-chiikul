import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import { View, StyleSheet, BackHandler } from 'react-native';
import { TextBody, ButtonMain } from '../../Components';
import { pallete, routes } from '../../constants';
import { connect } from 'react-redux';
import { appActions } from '../../actions/appActions';
import { appStore } from '../../selectors';
import { useFocusEffect } from '@react-navigation/native';
import { MainContent } from '../Layouts';



const ReviewInfo = ({ packageRequest, infoUser, typePerson, sendPackageRequest, navigation, clearAll, setFolio }) => {

  const handleNext = () => {
    navigation.replace(routes.SING);
    // let p = packageRequest;
    // p["solicitant"] = infoUser.id;
    // sendPackageRequest(p).then(result => {      
    //   setFolio(result.payload ? "".concat(result.payload.folio) : "");
    //   navigation.replace(routes.END_PROCEDURE);
    // }, error => {
    //   if (error === -1) {//token is not valid
    //     navigation.popToTop();
    //   }
    // });
  }

  const handleCancel = () => {
    clearAll().then(result => {
      navigation.popToTop();
    }, error => { })
  }

  useFocusEffect(
    useCallback(() => {
      const onBackPress = () => {
        handleCancel();
        return true;
      };

      BackHandler.addEventListener('hardwareBackPress', onBackPress);

      return () =>
        BackHandler.removeEventListener('hardwareBackPress', onBackPress);
    })
  );

  return (
    <View style={styles.root} >

      <View style={styles.body}>
        <TextBody textBold textCenter style={{ marginBottom: 15 }}  >{'Solicitud de Generación o Actualización de Contraseña.'}</TextBody>
        <MainContent >
          <View style={styles.content}>
            <TextBody textBold >{'Datos generales.'}</TextBody>
            <TextBody textMedium ><TextBody textBold textMedium >{'RFC: '}</TextBody>{infoUser && infoUser.rfc}</TextBody>
            <TextBody textMedium ><TextBody textBold textMedium >{'Teléfono: '}</TextBody>{infoUser && infoUser.telephone}</TextBody>
            <TextBody textMedium ><TextBody textBold textMedium >{'Email: '}</TextBody>{infoUser && infoUser.email}</TextBody>
          </View>
          <View style={styles.content} >
            <TextBody textBold >{'Documentación.'}</TextBody>
            {/* {
              typePerson && Array.isArray(typePerson.general_documents) && typePerson.general_documents.map((item) => (
                <TextBody textMedium key={item.id} >- {item.name && item.name}</TextBody>
              ))
            } */}
            <TextBody textMedium >- {'Identificación Oficial'}</TextBody>
            {/* <TextBody textMedium >- {'Solicitud de Contraseña'}</TextBody> */}
            <TextBody textMedium >- {'Video de identidad'}</TextBody>
          </View>
          <View style={styles.content} >
            <TextBody textBold textParagraph >{'Manifestaciones.'}</TextBody>
            <TextBody textMedium >El que suscribe, manifiesto que:</TextBody>
            <TextBody textMedium textJustify >
              Soy mayor de edad, que actualmente me encuentro inscrito en el Régimen de Sueldos y Salarios e Ingresos Asimilados a Salarios y que solicito al Servicio de Administración Tributaria apegarme a la facilidad contenida en la regla 2.2.1 Valor probatorio de la Contraseña contenida en la Resolución Miscelánea Fiscal vigente, respecto del servicio de Generación o Actualización de Contraseña a través de la Aplicación Móvil.
              Es de mi conocimiento y conformidad que, con el propósito de brindar seguridad jurídica para el uso de este servicio, enviaré al “SAT” a través de los medios que establezca los requisitos señalados en la ficha de trámite 7/CFF Solicitud de generación y actualización de la Contraseña del Anexo 1-A de la Resolución Miscelánea Fiscal Vigente y que dichos documentos en conjunto con mis datos de identidad, arriba especificados, servirán para acreditar mi identidad, para lo cual el “SAT”, realizará el cotejo y verificación de los mismo para determinar la procedencia de la solicitud en un tiempo 3  días hábiles contados a partir del día siguiente del envío de la solicitud.
              Declaro bajo protesta de decir verdad que las imágenes de la documentación e información que presento para cotejo y verificación corresponde a mi persona y es auténtica, manifiesto que todos los datos asentados en la presente solicitud son verídicos y exactos, así mismo que soy conocedor de las penas en que incurren la personas que declaran con falsedad en los términos de lo dispuesto por el artículo 247, fracción I del Código Penal Federal, en relación con el artículo 110 fracción II del Código Fiscal de la Federación.
              A través de este medio autorizó al Servicio de Administración Tributaria el envío de notificaciones para el uso del servicio a mi número de teléfono móvil y correo electrónico arriba manifestados los cuales son de mi dominio y exclusivamente de mi uso personal, así mismo autorizo la actualización de mis datos de contacto en las bases de datos de los sistemas del “SAT”.
              Es de mi conocimiento que la autoridad realizará verificaciones tanto de documentación aportada como de mi situación fiscal de conformidad con la regla 2.2.1 Valor probatorio de la Contraseña de la Resolución Miscelánea Fiscal vigente, para determinar la procedencia del uso del servicio.
              Es de mi conocimiento que en caso de que la autoridad me determine que mi solicitud no es procedente, únicamente podré realizar la solicitud por un máximo de tres intentos a través de este medio, de lo contrario generaré o actualizaré mi Contraseña a través de los otros medios existentes para la obtención o actualización de la Contraseña, así mismo manifiesto que el “SAT”, no será responsable por daños y perjuicios que puedan registrarse a mí o a terceros, por la eventual imposibilidad de realizar la presentación a través de este servicio.
              En caso de que mi solicitud sea procedente  autorizó al “SAT” el envío de notificaciones a mi número de teléfono móvil y correo electrónico, arriba manifestados para que a través de la liga que se me proporcione, genere o actualice mi Contraseña.
              Conozco y acepto que soy responsable del resguardo y uso de la  de la Contraseña; así como el único conocedor y absoluto responsable del uso de la ésta, y que tengo conocimiento que la Contraseña que establezca forma parte íntegra de la  firma autógrafa y produce los mismos efectos que las leyes otorgan a los documentos correspondientes, teniendo igual valor probatorio.
            </TextBody>
          </View>
        </MainContent>
      </View>

      <View style={styles.footer}>
        <ButtonMain buttonRed buttonPadding buttonCenter text={'Reiniciar proceso'} onPress={handleCancel} />
        <ButtonMain buttonBlue buttonPadding buttonCenter style={{ paddingLeft: 37, paddingRight: 37, marginTop: 5 }}
          text={'Firmar solicitud'} onPress={handleNext} />
      </View>
    </View>
  );

}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    padding: 15,
    fontSize: 22,
    backgroundColor: '#f5f5f5',
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  body: {
    flex: 10,
    fontSize: 22,
  },
  footer: {

  },
  solicitude: {
    marginBottom: 45,
  },
  textInput: {
    backgroundColor: "#FFFFFF",
    height: 40,
    borderRadius: 10,
    paddingLeft: 10,
    borderColor: 'gray', borderWidth: 1
  },
  content: {
    backgroundColor: "#FFFFFF",
    borderRadius: 15,
    paddingBottom: 15,
    paddingRight: 15,
    paddingLeft: 15,
    paddingTop: 15,
    marginTop: 15,
  },
});

const mapStateToProps = (state) => ({
  packageRequest: appStore.getPackageRequest(state),
  infoUser: appStore.getInfoUser(state),
  typePerson: appStore.getTypePerson(state),
});

const mapDispathToProps = dispatch => ({
  sendPackageRequest: (payload) => dispatch(appActions.sendPackageRequest(payload)),
  clearAll: (payload) => dispatch(appActions.clearAll(payload)),
  setFolio: (payload) => dispatch(appActions.setFolio(payload)),
});

ReviewInfo.propTypes = {

};

export default connect(mapStateToProps, mapDispathToProps)(ReviewInfo);