import React from "react";
import Svg, { G, Path } from "react-native-svg";


const Replay = ({ height, width, color, ...style }) => {
  return (
    <Svg
      height={height} width={width} {...style} fill={color}
      xmlns="http://www.w3.org/2000/svg" xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
      viewBox="0 0 472.615 472.615" style="enable-background:new 0 0 472.615 472.615;" space="preserve">
      <G>
        <G>
          <Path d="M355.232,0l-13.525,13.525l65.821,65.821h-279.17c-52.894,0-95.924,43.031-95.924,95.919v59.633h19.128v-59.633
			c0-42.343,34.452-76.79,76.796-76.79h279.17l-65.821,65.821l13.525,13.525l88.91-88.91L355.232,0z"/>
        </G>
      </G>
      <G>
        <G>
          <Path d="M421.053,237.714v59.632c0,42.344-34.452,76.795-76.796,76.795H65.087l65.821-65.825l-13.525-13.525l-88.909,88.914
			l88.909,88.91l13.525-13.525L65.087,393.27h279.17c52.895,0,95.924-43.031,95.924-95.924v-59.632H421.053z"/>
        </G>
      </G>
      <G>
      </G>
      <G>
      </G>
      <G>
      </G>
      <G>
      </G>
      <G>
      </G>
      <G>
      </G>
      <G>
      </G>
      <G>
      </G>
      <G>
      </G>
      <G>
      </G>
      <G>
      </G>
      <G>
      </G>
      <G>
      </G>
      <G>
      </G>
      <G>
      </G>
    </Svg>


  );
};

Replay.defaultProps = {
  width: 32,
  height: 32,
  color: "#FFFFFF"

}

export default Replay;