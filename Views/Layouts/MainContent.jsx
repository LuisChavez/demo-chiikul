import React from 'react';
import PropTypes from 'prop-types';
import { ScrollView } from 'react-native-gesture-handler';
import { StyleSheet } from 'react-native';


const MainContent = ({ children }) => {
  return (
    <ScrollView style={styles.root} >
      {children && children}
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 1,   
  }
})

MainContent.propTypes = {

};

export default MainContent;