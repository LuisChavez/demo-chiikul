import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, StyleSheet, Modal, ScrollView, BackHandler, Linking } from 'react-native';
import { TextBody, ButtonMain, Divider } from '../../Components';
import { url_document, routes } from '../../constants';
import MainContent from '../Layouts/MainContent';
import { connect } from 'react-redux';
import { appActions } from '../../actions/appActions';
import { appStore } from '../../selectors';


class Presentation extends Component {

  constructor(props) {
    super(props);
    this.state = {
      showInfo: false,
      curretType: {}
    };
  }


  handleOpenDocument = () => {
    // Checking the link
    Linking.canOpenURL(url_document).then(supported => {
      Linking.openURL(url_document);
    });
  };


  handleCloseInfo = () => {
    this.setState({ showInfo: false, type: {} });
  }

  handleOpenInfo = (type) => {
    this.setState({
      showInfo: true,
      curretType: type
    });
  }

  handleInit = () => {
    const { navigation } = this.props;
    navigation.replace(routes.PRESENTATION);
  }

  handleOut = () => {
    BackHandler.exitApp()
  }

  componentDidMount() {
    this.props.fetchTaxpayerList().then(result => {
    }, error => {
      this.handleBack();
    });
  }

  render() {

    const { showInfo, curretType } = this.state;
    const { taxpayerList } = this.props;

    return (
      <React.Fragment>
        <View style={styles.root} >
          <View style={styles.body}>
            <MainContent>
              <TextBody textParagraph textBold textCenter  >
                {'Para poder generar o actualizar la contraseña deberás enviar al SAT la siguiente documentación:'}
              </TextBody>
              <TextBody textBold >
                {'•	Identificación oficial'}
              </TextBody>
              <TextBody style={{ paddingLeft: 15 }} >
                {'∘	Credencial para votar vigente,  (INE). '}
              </TextBody>
              <TextBody style={{ paddingLeft: 15 }} >
                {'∘ Pasaporte Vigente. '}
              </TextBody>
              <TextBody textParagraph style={{ paddingLeft: 15 }} >
                {'∘ Cédula profesional con fotografía.'}
              </TextBody>

              {/* <TextBody textBold  >
                {'•	Solcitud de generación o actualización de Contraseña '}
                <TextBody textBold textSubline textBlue onPress={this.handleOpenDocument}  >
                  {' (descarga formato)'}
                </TextBody>
              </TextBody>
              <TextBody style={{ paddingLeft: 15 }} >
                {'∘ Deberás llenar los datos indicados'}
              </TextBody>
              <TextBody style={{ paddingLeft: 15 }} >
                {'∘ Debe ser firmado en tinta azul'}
              </TextBody> */}
              {Array.isArray(taxpayerList) && taxpayerList.length > 1 &&
                <React.Fragment>
                  <TextBody textBold textBlue >{'Personas físicas.'}</TextBody>
                  {taxpayerList.map((t) => {
                    return <TextBody key={t.id} textSubline textBlue onPress={() => this.handleOpenInfo(t)} style={{ paddingLeft: 15 }} >
                      {t.name}
                    </TextBody>
                  })}
                </React.Fragment>
              }
            </MainContent>

          </View>
          <View style={styles.footer}>
            <ButtonMain buttonBig buttonBlue buttonCenter text={'INICIAR TRÁMITE'} onPress={this.handleInit} />
            <ButtonMain buttonBig buttonRed buttonCenter text={'Salir'} onPress={this.handleOut}
              style={{ paddingLeft: 107, paddingRight: 107 }}
            />
          </View>
        </View>

        <Modal style={styles.modal}
          animationType="slide"
          transparent={true}
          visible={showInfo}>
          <View style={styles.modalRoot} >
            <View style={styles.modalBody} >
              <TextBody textBlue textSubline >
                {`Documentación para ${curretType ? curretType.name : ""}.`}
              </TextBody>
              <Divider />
              <ScrollView >
                {curretType && Array.isArray(curretType.general_documents) &&
                  curretType.general_documents.map((t, index) => (
                    <React.Fragment key={index} >
                      <TextBody textBold >
                        {`${index + 1}. ${t.name}`}
                      </TextBody>
                      {Array.isArray(t.types) && t.types.map((d, i) => (
                        <TextBody key={d.name} textMedium >
                          <TextBody textMedium >
                            {'- '}
                            {d.description && d.description}
                          </TextBody>
                        </TextBody>
                      ))
                      }
                    </React.Fragment>
                  ))
                }

                {
                  curretType && Array.isArray(curretType.specific_documents) &&
                  curretType.specific_documents.map((t, index) => (
                    <React.Fragment key={index} >
                      <TextBody textBold >
                        {`${curretType && Array.isArray(curretType.general_documents) ? curretType.general_documents.length + index + 1 : index + 1}. ${t.name}`}
                      </TextBody>
                      {t.description &&
                        <TextBody textMedium >
                          {t.description}
                        </TextBody>
                      }
                    </React.Fragment>
                  ))
                }
              </ScrollView>
              <ButtonMain
                buttonBig buttonBlue buttonCenter
                onPress={this.handleCloseInfo}
                text={'Regresar'} />
            </View>
          </View>
        </Modal>

      </React.Fragment>
    );
  }
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    padding: 15,
    fontSize: 22,
    backgroundColor: '#f5f5f5',
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  body: {
    flex: 19,
    fontSize: 22,
  },
  footer: {
    flex: 5,
    justifyContent: "center",
  },
  modal: {
    flex: 1,
  },
  modalRoot: {
    flex: 1,
    padding: 15,
    paddingBottom: 30,
    paddingTop: 30,
    backgroundColor: '#000000CB',
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  modalBody: {
    padding: 20,
    alignContent: "center",
    justifyContent: "center",
    backgroundColor: "#FFFFFF",
    borderRadius: 30,
    marginBottom: 30,
    marginTop: 30,
  },
  msj: {
    textAlign: "center"
  },
});

const mapStateToProps = (state) => ({
  taxpayerList: appStore.taxpayerList(state),
});

const mapDispathToProps = dispatch => ({
  fetchTaxpayerList: (payload) => dispatch(appActions.fetchTaxpayerList()),
});

Presentation.defaultProps = {

}

Presentation.propTypes = {

};

export default connect(mapStateToProps, mapDispathToProps)(Presentation);