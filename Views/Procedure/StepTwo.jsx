import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { View, StyleSheet, BackHandler } from 'react-native';
import { TextBody, ButtonMain } from '../../Components';
import { routes } from '../../constants';
import { appStore } from '../../selectors';
import ReviewDoc from './ReviewDoc';
import { MainContent } from '../Layouts';

const sourceMedia = {
  FILES: "FILES",
  CAMERA: "CAMERA",
}


class StepTwo extends Component {

  constructor(props) {
    super(props);
    this.state = {
      showMsg: false,
      showDialogConfirm: false,
      source: undefined,
    }
  }

  handleOpenMessage = () => {
    this.setState({ showMsg: true });
  }

  handlePhotos = () => {
    this.setState({ source: sourceMedia.CAMERA });
    this.showConfirmDocument(true);
  }

  handleFiles = () => {
    this.setState({ source: sourceMedia.FILES });
    this.showConfirmDocument(true);
  }

  getCurrentDocument = () => {
    const { typePerson, documentIndex } = this.props;
    return typePerson && Array.isArray(typePerson.general_documents) && typePerson.general_documents[documentIndex] ? typePerson.general_documents[documentIndex] : {};
  }

  handleDocumentCancel = () => {
    this.showConfirmDocument(false);
  }

  handleDocumentOk = (typeSelected) => {
    const { navigation, typePerson, documentIndex } = this.props;
    const { source } = this.state;
    const nextStep = (Array.isArray(typePerson.general_documents) &&
      documentIndex < (typePerson.general_documents.length - 1)) ? routes.STEP_TWO : routes.IDENTITY;
    switch (source) {
      case sourceMedia.CAMERA:
        const documents = this.getCurrentDocument();
        navigation.navigate(routes.CAMERA_VIEW, {
          typeSelected: typeSelected,
          categoryDocument: this.getCurrentDocument().id,
          nextStep: nextStep
        });
        setTimeout(() => {
          this.showConfirmDocument(false);
        }, 100);
        break;
      case sourceMedia.FILES:
        navigation.navigate(routes.FILES, {
          typeSelected: typeSelected,
          categoryDocument: this.getCurrentDocument().id,
          nextStep: nextStep
        });
        setTimeout(() => {
          this.showConfirmDocument(false);
        }, 100);
        break;
      default:
        break;
    }
  }

  showConfirmDocument = (show) => {
    this.setState({ showDialogConfirm: show })

  }

  onBackPress = () => {
    const { navigation } = this.props;
    navigation.popToTop();
    return true;
  };

  componentDidMount() {        
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
  }




  render() {
    const { showMsg, showDialogConfirm } = this.state;
    const currentDocument = this.getCurrentDocument();

    return (
      <React.Fragment>
        <MainContent>
          <View style={styles.root} >
            <View style={styles.head}>
              <TextBody textCenter style={styles.textTitle} >{'Integra la documentación requerida para acreditar tu identidad:'}</TextBody>
              <TextBody textCenter style={styles.textBody} textParagraph >{'Selecciona la opción desde la cual agregaras los documentos: '}
                <TextBody style={{ fontWeight: "bold", }} >
                  {currentDocument && currentDocument.name}
                </TextBody>.
              </TextBody>
            </View>
            <View style={styles.body}>
              <ButtonMain buttonBlue buttonCenter buttonPadding text={'CÁMARA'} onPress={this.handlePhotos} />
              <ButtonMain buttonBlue buttonCenter buttonPadding text={'ARCHIVO'} onPress={this.handleFiles} />
            </View>
          </View>
        </MainContent>

        <ReviewDoc
          nameDocument={currentDocument.name || ""}
          listDocuments={this.getCurrentDocument().types}
          show={showDialogConfirm}
          fnOk={this.handleDocumentOk}
          fnCancel={this.handleDocumentCancel} />

      </React.Fragment>
    );
  }
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    padding: 15,
    fontSize: 22,
    backgroundColor: '#f5f5f5',
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  head: {
    flex: 1,
    justifyContent: "center"
  },
  body: {
    flex: 2,
    justifyContent: "center",
  },
  textTitle: {
    fontWeight: "bold",
    marginBottom: 45,
  },
  textBody: {

  },
  modalRoot: {
    flex: 1,
  },
  modalContent: {
    flex: 1,
    padding: 15,
    backgroundColor: '#000000CB',
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  modalBody: {
    padding: 20,
    alignContent: "stretch",
    justifyContent: "center",
    backgroundColor: "#FFFFFF",
    borderRadius: 30,
  },
});

const mapStateToProps = (state) => ({
  taxpayerDictionary: appStore.taxpayerDictionary(state),
  typePerson: appStore.getTypePerson(state),
  documentIndex: appStore.getIndexDocument(state),
});

const mapDispathToProps = dispatch => ({
});

StepTwo.defaultProps = {
};

StepTwo.propTypes = {

};

export default connect(mapStateToProps, mapDispathToProps)(StepTwo);