import React from 'react';
import PropTypes from 'prop-types';
import { View, ActivityIndicator, Modal, StyleSheet } from 'react-native';
import { pallete } from '../../constants';
import { connect } from 'react-redux';
import { appStore } from '../../selectors';

const Loading = ({ showLoading, ...props }) => {
  
  return (
    <Modal style={styles.root}
      transparent={true}
      visible={showLoading}>
      <View style={styles.content} >
        <View style={styles.body} >
          <ActivityIndicator size="large" color={pallete.main} />
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
  },
  content: {
    flex: 1,
    padding: 15,
    backgroundColor: '#000000CB',
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  body: {
    padding: 20,
    alignContent: "stretch",
    justifyContent: "center",
    borderRadius: 30,
  },
})

const mapStateToProps = (state) => ({
  showLoading: appStore.getIsLoading(state),
});

Loading.propTypes = {
  showLoading: PropTypes.bool,
};

export default connect(mapStateToProps, null)(Loading);