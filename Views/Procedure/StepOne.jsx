import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { View, StyleSheet } from 'react-native';
import { TextBody, ButtonMain, ModalCaptcha, ModalInput } from '../../Components';
import { pallete, routes } from '../../constants';
import { appStore } from '../../selectors';
import { appActions } from '../../actions/appActions';
import { getTokenUser, isValidated, getStorageUser } from '../../helpers';


const FIELD = {
  RFC: "RFC",
  PHONE: "PHONE",
  EMAIL: "EMAIL"
}

class StepOne extends Component {

  constructor(props) {
    super(props);
    this.state = {
      showCaptcha: false,
      captcha: {},
      currentField: "RFC",
      showInput: false,
      msgInput: "",
      titleInput: "",
      form: {
        RFC: { text: "", keyboardType: "default", max: 13, autoCapitalize: "characters", msg: "" },
        PHONE: { text: "", keyboardType: "phone-pad", max: 10, autoCapitalize: "none", msg: "" },
        EMAIL: { text: "", keyboardType: "email-address", max: 255, autoCapitalize: "none", msg: "" }
      }
    }
  }

  validaRFC = (rfc) => {
    const re = /^([A-ZÑ&a-zñ&]{4}) ?(?:- ?)?(\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])) ?(?:- ?)?([A-Za-z\d]{2})([Aa\d])$/;
    const resp = rfc.match(re);
    return (resp !== null);  //Coincide con el formato general del regex?
  }

  handleChangeTextRfc = (text) => {
    const { setSolicitant, solicitant } = this.props;
    if (text.length <= 13) {
      let sol = solicitant;
      sol.rfc = text;
      setSolicitant(sol);
    }
  }

  handleChangeTextPhone = (text) => {
    const { setSolicitant, solicitant } = this.props;
    if (text.length <= 10) {
      let sol = solicitant;
      sol.telephone = text;
      setSolicitant(sol);
    }
  }

  handleChangeTextEmail = (text) => {
    const { setSolicitant, solicitant } = this.props;
    let sol = solicitant;
    sol.email = text;
    setSolicitant(sol);
  }

  handleOpenModal = () => {
    const { fetchCaptcha } = this.props;
    fetchCaptcha().then(result => {
      this.setState({
        showCaptcha: true,
        captcha: result
      });
    }, error => {

    })
  }

  handleOpenDialogInput = (field) => {
    let msgInput = "", titleInput = "";
    switch (field) {
      case FIELD.EMAIL:
        msgInput = "";
        titleInput = "Captura tu correo electrónico personal.";
        break;
      case FIELD.RFC:
        msgInput = "";
        titleInput = "Captura tu RFC a 13 posiciones.";
        break;
      case FIELD.PHONE:
        msgInput = "";
        titleInput = "Captura el número de tu teléfono celular a 10 posiciones.";
        break;
      default: break;
    }


    this.setState({
      showInput: true,
      currentField: field,
      msgInput: msgInput,
      titleInput: titleInput,
    })
  }

  handleChangeField = (text) => {
    const { setSolicitant, solicitant } = this.props;
    const { currentField } = this.state;
    let sol = solicitant, msg = "";
    let { form } = this.state;
    form[currentField].text = text;

    if (text.length > 0)
      switch (currentField) {
        case FIELD.EMAIL:
          if (text.indexOf("@") < 1 || text.indexOf(".") < 3) {
            msg = "Introduce una dirección de correo valida.";
          }
          sol.email = text;
          form[currentField].msg = msg;
          setSolicitant(sol);
          break;
        case FIELD.RFC:
          if (text.length < 13 || !this.validaRFC(text)) {
            msg = "Introduce un RFC valido para persona Física.";
          }
          form[currentField].msg = msg;
          sol.rfc = text;
          setSolicitant(sol);
          break;
        case FIELD.PHONE:
          if (text.length < 10) {
            msg = "Introduce los 10 digitos del número de teléfono.";
          }
          form[currentField].msg = msg;
          sol.telephone = text;
          setSolicitant(sol);
          break;
        default: break;
      }

    this.setState({
      showInput: false,
      form: form
    })
  }

  fnReloadCaptcha = () => {
    const { fetchCaptcha } = this.props;
    fetchCaptcha().then(result => {
      this.setState({
        captcha: result
      });
    }, error => {

    })
  }

  fnSendForm = (textCaptcha) => {
    const { registerSolicitant, solicitant, navigation, setInfoUser, setSolicitant } = this.props;
    const { captcha } = this.state;

    let sol = solicitant;
    sol.catpcha_id = captcha.id;
    sol.catpcha_res = textCaptcha;
    setSolicitant(sol);

    this.setState({ showCaptcha: false });
    registerSolicitant(sol).then(data => {
      // TODO: Mostrar mensaje de registo ok      
      getTokenUser().then(tkn => {
        getStorageUser().then(info => {
          tkn.rfc = info.rfc;
          tkn.email = info.email;
          tkn.telephone = info.telephone;

          setInfoUser(tkn);
          navigation.replace(routes.STEP_ONE_SMS);
        });
      }, error => {//no info
        //navigation.goBack();
        //setError(true,"No fue posible obtener la información del actual usuario");
      })
    }, error => {

    });
  }

  async componentDidMount() {
    const { navigation, fetchTaxpayerList, setTypePerson, setIndexDocument, taxpayerOptions, solicitant } = this.props;
    fetchTaxpayerList().then(result => { }, error => {
      navigation.goBack();
    });
    // Exist token
    const tkn = await getTokenUser();
    if (tkn && tkn.id !== undefined) {
      const info = await getStorageUser();
      const { setInfoUser } = this.props;
      tkn.rfc = info.rfc;
      tkn.email = info.email;
      tkn.telephone = info.telephone;

      setInfoUser(tkn);
      const isValid = await isValidated();
      if (isValid === "true") {
        setTypePerson(taxpayerOptions[0].value);
        setIndexDocument(0);
        navigation.replace(routes.STEP_TWO);
        //navigation.replace(routes.STEP_ONE_TYPE);
      }
      else {
        navigation.replace(routes.STEP_ONE_SMS);
      }
    }
  }

  render() {
    const { showCaptcha, captcha, showInput, msgInput, titleInput, form, currentField } = this.state;
    const { solicitant } = this.props;

    return (
      <React.Fragment>
        <View style={styles.root} >
          <View style={styles.body}>
            <TextBody textCenter textBold style={{ paddingBottom: 45 }}  >{'Ingresa los siguientes datos.'}</TextBody>
            <TextBody textBold >{'RFC:'}</TextBody>
            <TextBody textSmall style={styles.textInput} adjustsFontSizeToFit={true} includeFontPadding={true}
              onPress={() => this.handleOpenDialogInput(FIELD.RFC)}
            >
              {form[FIELD.RFC].text}
            </TextBody>
            <TextBody textSmall textRed >{form[FIELD.RFC].msg}</TextBody>
            <TextBody style={{ paddingTop: 15 }} textBold >{'Teléfono:'}</TextBody>
            <TextBody textSmall style={styles.textInput} adjustsFontSizeToFit={true} includeFontPadding={true}
              onPress={() => this.handleOpenDialogInput(FIELD.PHONE)}
            >
              {form[FIELD.PHONE].text}
            </TextBody>
            <TextBody textSmall textRed >{form[FIELD.PHONE].msg}</TextBody>
            <TextBody style={{ paddingTop: 15 }} textBold >{'Email:'}</TextBody>
            <TextBody textSmall style={styles.textInput} adjustsFontSizeToFit={true} includeFontPadding={true}
              onPress={() => this.handleOpenDialogInput(FIELD.EMAIL)}
            >
              {form[FIELD.EMAIL].text}
            </TextBody>
            <TextBody textSmall textRed >{form[FIELD.EMAIL].msg}</TextBody>
          </View>
          <View style={styles.footer}>
            <ButtonMain
              buttonBlue
              buttonCenter
              buttonPadding
              text={'CONTINUAR'} onPress={this.handleOpenModal} disabled={
                (form[FIELD.RFC].msg.length > 0) ||
                (form[FIELD.EMAIL].msg.length > 0) ||
                (form[FIELD.PHONE].msg.length > 0) ||
                (form[FIELD.RFC].text.length === 0) ||
                (form[FIELD.EMAIL].text.length === 0) ||
                (form[FIELD.PHONE].text.length === 0)

              }
            />
          </View>
        </View>
        <ModalCaptcha
          show={showCaptcha}
          fnOk={this.fnSendForm}
          image={captcha.img}
          fnChange={this.fnReloadCaptcha}
        />
        <ModalInput
          show={showInput}
          fnOk={this.handleChangeField}
          msg={msgInput}
          title={titleInput}
          textInit={form[currentField].text}
          typeInput={currentField}
          keyboardType={form[currentField].keyboardType}
          max={form[currentField].max}
          autoCapitalize={form[currentField].autoCapitalize}
        />
      </React.Fragment>
    );
  }
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    padding: 15,
    fontSize: 22,
    backgroundColor: '#f5f5f5',
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  body: {
    flex: 10,
    fontSize: 22,
  },
  footer: {
    flex: 2,
    justifyContent: "center"
  },
  buttonBlue: {
    backgroundColor: pallete.main,
    alignSelf: "center",
    justifyContent: "center",
    padding: 40,
    paddingTop: 10,
    paddingBottom: 10,
  },
  textInput: {
    backgroundColor: "#FFFFFF",
    height: 40,
    borderRadius: 10,
    paddingLeft: 10,
    borderColor: 'gray',
    borderWidth: 1,
    textAlignVertical: "center",
    paddingTop: 10,
    paddingBottom: 10,
  }
});

const mapStateToProps = (state) => ({
  solicitant: appStore.getSolicitant(state),
  solicitantRFC: appStore.getRFC(state),
  solicitantEmail: appStore.getEmail(state),
  solicitantTelephone: appStore.getTelephone(state),
  taxpayerOptions: appStore.taxpayerOptions(state),
});

const mapDispathToProps = dispatch => ({
  setSolicitant: (payload) => dispatch(appActions.setSolicitant(payload)),
  registerSolicitant: (payload) => dispatch(appActions.registerSolicitant(payload)),
  setInfoUser: (payload) => dispatch(appActions.setInfoUser(payload)),
  fetchTaxpayerList: (payload) => dispatch(appActions.fetchTaxpayerList()),
  fetchCaptcha: (payload) => dispatch(appActions.fetchCaptcha()),

  setTypePerson: (payload) => dispatch(appActions.setTypePerson(payload)),
  setIndexDocument: (payload) => dispatch(appActions.setIndexDocument(payload)),

});

StepOne.defaultProps = {
  solicitant: {},
}

StepOne.propTypes = {
  solicitant: PropTypes.object,
};

export default connect(mapStateToProps, mapDispathToProps)(StepOne);