import { createSelector } from 'reselect';


const app = state => { return state.app !== undefined ? state.app : {}; }
const appSolicitant = app => { return app.solicitant !== undefined ? app.solicitant : {}; }
const appInfoUser = app => { return app.info_user !== undefined ? app.info_user : {}; }
const appTaxpayerList = app => { return app.taxpayer_list !== undefined ? app.taxpayer_list : []; }
const appTypePerson = app => { return app.type_person !== undefined ? app.type_person : {}; }
const appPackageRequest = app => { return app.package_request !== undefined ? app.package_request : {}; }
const appGetIsLoading = app => { return app.isLoading !== undefined ? app.isLoading : false; }
const appError = app => { return app.error !== undefined ? app.error : {}; }
const appIndex = app => { return app.index_document !== undefined ? app.index_document : 0 }
const appFolio = app => { return app.folio !== undefined ? app.folio : "" }

export const getSolicitant = createSelector(
  app,
  (appStore) => (appSolicitant(Object.assign({}, appStore)))
)

export const getInfoUser = createSelector(
  app,
  (appStore) => (appInfoUser(Object.assign({}, appStore)))
)

export const taxpayerList = createSelector(
  app,
  (appStore) => (appTaxpayerList(Object.assign({}, appStore)))
)

export const taxpayerOptions = createSelector(
  taxpayerList,
  (list) => {
    const data = list;
    return data.reduce((last, current) => {
      last.push({
        label: current.name,
        value: current,
      });
      return last;
    }, []);
  }
)

export const taxpayerDictionary = createSelector(
  app,
  (appStore) => {
    const data = appTaxpayerList(appStore);
    return data.reduce((last, current) => {
      last[current.id] = current;
      return last;
    }, {});
  }
)

export const getTypePerson = createSelector(
  app,
  (appStore) => (appTypePerson(Object.assign({}, appStore)))
)

export const getPackageRequest = createSelector(
  app,
  (appStore) => (appPackageRequest(Object.assign({}, appStore)))
)

export const getIsLoading = createSelector(
  app,
  (appStore) => (appGetIsLoading(appStore))
)

export const getShowError = createSelector(
  app,
  (appStore) => (appError(appStore).show)
)

export const getInfoError = createSelector(
  app,
  (appStore) => (appError(appStore).info)
)

export const getIndexDocument = createSelector(
  app,
  (appStore) => (appIndex(appStore))
)

export const getFolio = createSelector(
  app,
  (appStore) => (appFolio(appStore))
)






export const getRFC = createSelector(
  app,
  (appStore) => (appSolicitant(appStore).rfc)
)

export const getTelephone = createSelector(
  app,
  (appStore) => (appSolicitant(appStore).telephone)
)

export const getEmail = createSelector(
  app,
  (appStore) => (appSolicitant(appStore).email)
)

export const getCatpchaId = createSelector(
  app,
  (appStore) => (appSolicitant(appStore).catpcha_id)
)

export const getCatpchaRes = createSelector(
  app,
  (appStore) => (appSolicitant(appStore).catpcha_res)
)

