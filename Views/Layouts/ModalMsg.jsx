import React from 'react';
import PropTypes from 'prop-types';
import { Modal, View, StyleSheet } from 'react-native';
import { TextBody, ButtonMain } from '../../Components';
import { connect } from 'react-redux';
import { appStore } from '../../selectors';
import { appActions } from '../../actions/appActions';

const styles = StyleSheet.create({
  modal: {
    flex: 1,
  },
  root: {
    flex: 1,
    padding: 15,
    backgroundColor: '#000000CB',
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  body: {
    padding: 20,
    alignContent: "center",
    justifyContent: "center",
    backgroundColor: "#FFFFFF",
    borderRadius: 15,
  },
});

const ModalMsg = ({ setError, showError, infoError }) => {

  const handleCloseError = () => {
    setError(false, "");
  }

  return (
    <Modal style={styles.modal}
      animationType="slide"
      transparent={true}
      visible={showError}>
      <View style={styles.root} >
        <View style={styles.body} >
          <TextBody textCenter textSmall >
            {infoError && infoError}
          </TextBody>
          <ButtonMain
            buttonBlue
            buttonCenter
            buttonPadding
            onPress={handleCloseError}
            text={'CERRAR'} />
        </View>
      </View>
    </Modal>
  );

}




const mapStateToProps = (state) => ({
  showError: appStore.getShowError(state),
  infoError: appStore.getInfoError(state),
});

const mapDispathToProps = dispatch => ({
  setError: (show, info) => dispatch(appActions.setError({ info, show }))
});

ModalMsg.propTypes = {

};

export default connect(mapStateToProps, mapDispathToProps)(ModalMsg);