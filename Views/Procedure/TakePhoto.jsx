import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { CameraView, TextBody } from '../../Components';
import { routes } from '../../constants';
import { connect } from 'react-redux';
import { appStore } from '../../selectors';
import { appActions } from '../../actions/appActions';

class TakePhoto extends Component {

  constructor(props) {
    super(props);
    this.state = {
      documents: [],
      documentIndex: 0,
    }
  }

  handleDocumentOk = (files) => {
    const { typeSelected = {}, categoryDocument = "", nextStep = routes.IDENTITY } = this.props.route.params;
    const { setPackageRequest, packageRequest, navigation, setIndexDocument, indexDocument } = this.props;
    let p = packageRequest;
    p[`${categoryDocument}-${typeSelected.id}`] = files.reduce((last, current, index) => {
      last.push({
        uri: current.uri,
        type: current.type === undefined ? "image/*" : current.type + "/*",
        name: `${categoryDocument}-${typeSelected.id}-index` + current.uri.substring(current.uri.lastIndexOf("."))
      })
      return last;
    }, []);
    setPackageRequest(Object.assign({}, p));
    setIndexDocument(indexDocument + 1);
    navigation.replace(nextStep);
  }

  componentDidMount() {
    const { taxpayerDictionary, route } = this.props;
    const { taxpayerType } = route.params;
    this.setState({ documents: taxpayerDictionary[taxpayerType] ? taxpayerDictionary[taxpayerType].general_documents : [] });
  }


  render() {
    const { showDialogConfirm, documents, documentIndex } = this.state;
    const { typeSelected = {}, categoryDocument = "" } = this.props.route.params;

    return (
      <React.Fragment>
        <CameraView
          blockMessage={
            <TextBody style={{ textAlign: "center", marginBottom: 30 }}>
              {`Realiza la toma de la${typeSelected.pages > 1 ? 's' : ''} fotografía${typeSelected.pages > 1 ? 's' : ''} de tu documento y al terminar pulsa el botón Continuar`}
              {/* <TextBody textBold >
                {typeSelected && typeSelected.name}
              </TextBody> */}
            </TextBody>
          }
          totalPhotos={typeSelected.pages || 1}
          handleTakePicture={this.handleDocumentOk}
        />
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  taxpayerDictionary: appStore.taxpayerDictionary(state),
  packageRequest: appStore.getPackageRequest(state),
  indexDocument: appStore.getIndexDocument(state),
});

const mapDispathToProps = dispatch => ({
  setPackageRequest: (payload) => dispatch(appActions.setPackageRequest(payload)),
  setIndexDocument: (payload) => dispatch(appActions.setIndexDocument(payload)),
});

TakePhoto.propTypes = {

};

export default connect(mapStateToProps, mapDispathToProps)(TakePhoto);