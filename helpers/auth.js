import { AsyncStorage } from "react-native";
import { Base64 } from "./base64";


const tokenKeyName = 'userInfo';
const validatedKeyName = 'isValidated';
const storageInfoUser = 'storageInfoUser';

export const clearTokenUser = async function () {
  await AsyncStorage.setItem(tokenKeyName, "");
}

export const getTokenUser = async function () {
  const tkn = await AsyncStorage.getItem(tokenKeyName);
  const jwt = tkn && tkn.length > 0 && tkn.split('.');
  return (jwt) ? JSON.parse(Base64.atob(jwt[1])) : undefined;
}

export const setTokenUser = function (token) {
  return AsyncStorage.setItem(tokenKeyName, token);
}

export const setStorageUser = function (info) { 
  return AsyncStorage.setItem(storageInfoUser, JSON.stringify(info));
}

export const setValidated = function (validated) {
  return AsyncStorage.setItem(validatedKeyName, validated);
}

const getToken = async function () {
  const tkn = await AsyncStorage.getItem(tokenKeyName);
  return tkn;
}

export const getStorageUser = async function () {
  const data = await AsyncStorage.getItem(storageInfoUser);  
  return (data) ? JSON.parse(data) : {};
}

export const isValidated = async function () {
  const validated = await AsyncStorage.getItem(validatedKeyName);
  return validated;
}

export async function authHeader() {
  // return authorization header with jwt token
  let user = await getToken();
  if (user) {
    return { 'Authorization': 'Token ' + user };
  } else {
    return {};
  }
}

export async function tokenIsValid() {
  let user = await getTokenUser();
  if (user.exp !== undefined) {
    return new Date(user.exp * 1000) > new Date();
  }
  else {
    return false;
  }
}