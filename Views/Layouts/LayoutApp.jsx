import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, StyleSheet, Image } from 'react-native';

class LayoutApp extends Component {
  render() {
    const { head, children } = this.props;
    return (
      <View style={styles.root} >
        <View style={styles.head} >
          <View style={styles.title} >
            {head && head}
          </View>
          <View style={styles.logo}  >
            <Image  style={styles.image}  
            
            source={require("../../assets/images/sat.png")} />
          </View>
        </View>
        <View style={styles.body} >
          {
            children && children
          }
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    paddingLeft:10,
    paddingRight:10,
    paddingBottom:10,
    
  },
  head: {
    flex:1,
    flexDirection:"row",    
  },
  body: {
    flex:9,
  },
  logo: {
    flex:4,    
  },
  title: {
    flex:6,    
    justifyContent:"center"
  },
  image:{
    resizeMode:"cover",
    flex:1,        
    width:150,
    alignSelf:"flex-end",
  }

});

LayoutApp.defaultProps = {

};

LayoutApp.propTypes = {
  head: PropTypes.node,

};

export default LayoutApp;