import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Files from './Files';
import { routes } from '../../constants';
import { connect } from 'react-redux';
import { appStore } from '../../selectors';
import { appActions } from '../../actions/appActions';

class SelectFiles extends Component {

  handleSelectFiles = (files) => {
    const { typeSelected = {}, categoryDocument = "", nextStep = routes.IDENTITY } = this.props.route.params;
    const { setPackageRequest, packageRequest, setIndexDocument, indexDocument, navigation } = this.props;
    let p = packageRequest;
    p[`${categoryDocument}-${typeSelected.id}`] = files.reduce((last, current, index) => {
      last.push({
        uri: current.uri,
        type: current.type + "/*",
        name: `${categoryDocument}-${typeSelected.id}-index` + current.uri.substring(current.uri.lastIndexOf("."))
      })
      return last;
    }, []);
    setPackageRequest(Object.assign({}, p));
    setIndexDocument(indexDocument + 1);
    navigation.replace(nextStep);
  }

  render() {
    const { typeSelected = {}, categoryDocument = "", nextStep } = this.props.route.params;    

    return (
      <React.Fragment>
        <Files
          typeSelected={typeSelected}
          nameDocument={typeSelected.name}
          fnOk={this.handleSelectFiles}
        />
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  packageRequest: appStore.getPackageRequest(state),
  indexDocument: appStore.getIndexDocument(state),
});

const mapDispathToProps = dispatch => ({
  setPackageRequest: (payload) => dispatch(appActions.setPackageRequest(payload)),
  setIndexDocument: (payload) => dispatch(appActions.setIndexDocument(payload)),
});

SelectFiles.propTypes = {

};

export default connect(mapStateToProps, mapDispathToProps)(SelectFiles);